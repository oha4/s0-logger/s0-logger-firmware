# Copyright (c) 2024 Albert Krenz <albert.krenz@mailbox.org>
# SPDX-License-Identifier: BSD-2-Clause-Patent

FROM registry.gitlab.com/oha4/devcontainer/zephyr-ci:3.5.0

ARG UID=1000
ARG GID=1000

USER root

COPY tools/JLink_Linux_V786_x86_64.deb .
RUN apt-get update && apt-get install -y cppcheck && apt-get install -y ./JLink_Linux_V786_x86_64.deb locales;exit 0 && rm -rf JLink_Linux_V786_x86_64.deb

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/'        /etc/locale.gen && \
    sed -i -e 's/# pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen


RUN groupadd -g $GID -o user
RUN useradd -u $UID -m -g user -G plugdev user \
	&& echo 'user ALL = NOPASSWD: ALL' > /etc/sudoers.d/user \
	&& chmod 0440 /etc/sudoers.d/user \
    && chown -R user:user /opt/zephyrproject/

USER user
ENV PATH="/opt/SEGGER/JLink/:$PATH"
