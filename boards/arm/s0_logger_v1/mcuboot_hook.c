/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 *
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "bootutil/image.h"
#include "bootutil/fault_injection_hardening.h"
#include "bootutil/boot_hooks.h"
#include "bootutil/mcuboot_status.h"
#include "bootutil/bootutil_log.h"
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>

BOOT_LOG_MODULE_REGISTER(action_hook);

static const struct gpio_dt_spec led0 = GPIO_DT_SPEC_GET(DT_ALIAS(ledcon), gpios);
static const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(DT_ALIAS(lederr), gpios);

void my_expiry_function(struct k_timer *timer_id){
  gpio_pin_toggle_dt(&led0);
  gpio_pin_toggle_dt(&led1);
}

K_TIMER_DEFINE(fwup_led_timer, my_expiry_function, NULL);

int init(){
  if(!gpio_is_ready_dt(&led0) || !gpio_is_ready_dt(&led1)){
    return -1;
  }

  if(gpio_pin_configure_dt(&led0, GPIO_OUTPUT_ACTIVE) != 0){
    return -1;
  }

  if(gpio_pin_configure_dt(&led1, GPIO_OUTPUT_ACTIVE) != 0){
    return -1;
  }

  return 0;
}

void mcuboot_status_change(mcuboot_status_type_t status){
  init();

	BOOT_LOG_INF("mcuboot status: %d ", status);
  switch(status){
    case MCUBOOT_STATUS_UPGRADING:
    {
      k_timer_start(&fwup_led_timer, K_MSEC(250), K_MSEC(250));
      break;
    }

    case MCUBOOT_STATUS_BOOTABLE_IMAGE_FOUND:
    {
      k_timer_stop(&fwup_led_timer);
      gpio_pin_set_dt(&led0, 0);
      gpio_pin_set_dt(&led1, 0);
      break;
    }

    case MCUBOOT_STATUS_NO_BOOTABLE_IMAGE_FOUND:
    {
      gpio_pin_set_dt(&led1, 1);
      break;
    }

    default:
    {
      break;
    }
  }

}