/*
 * Copyright (c) 2022 Nordic Semiconductor
 * SPDX-License-Identifier: Apache-2.0
 */

&pinctrl {
	uart0_default: uart0_default {
		group1 {
			psels = <NRF_PSEL(UART_TX, 0, 20)>,
				<NRF_PSEL(UART_RX, 0, 24)>;
		};
	};

	uart0_sleep: uart0_sleep {
		group1 {
			psels = <NRF_PSEL(UART_TX, 0, 20)>,
				<NRF_PSEL(UART_RX, 0, 24)>;
			low-power-enable;
		};
	};

  uart1_default: uart1_default {
		group1 {
			psels = <NRF_PSEL(UART_TX, 0, 6)>,
				<NRF_PSEL(UART_RX, 0, 8)>,
				<NRF_PSEL(UART_RTS, 0, 5)>,
				<NRF_PSEL(UART_CTS, 0, 4)>;
		};
	};

	uart1_sleep: uart1_sleep {
		group1 {
			psels = <NRF_PSEL(UART_TX, 0, 6)>,
				<NRF_PSEL(UART_RX, 0, 8)>,
				<NRF_PSEL(UART_RTS, 0, 5)>,
				<NRF_PSEL(UART_CTS, 0, 4)>;
			low-power-enable;
		};
	};

	qspi_default: qspi_default {
		group1 {
			psels = <NRF_PSEL(QSPI_SCK, 1, 9)>,
				<NRF_PSEL(QSPI_IO0, 0, 12)>,
				<NRF_PSEL(QSPI_IO1, 0, 21)>,
				<NRF_PSEL(QSPI_IO2, 0, 19)>,
				<NRF_PSEL(QSPI_IO3, 0, 7)>,
				<NRF_PSEL(QSPI_CSN, 0, 23)>;
		};
	};

	qspi_sleep: qspi_sleep {
		group1 {
			psels = <NRF_PSEL(QSPI_SCK, 1, 9)>,
				<NRF_PSEL(QSPI_IO0, 0, 12)>,
				<NRF_PSEL(QSPI_IO1, 0, 21)>,
				<NRF_PSEL(QSPI_IO2, 0, 19)>,
				<NRF_PSEL(QSPI_IO3, 0, 7)>,
				<NRF_PSEL(QSPI_CSN, 0, 23)>;
			low-power-enable;
		};
	};

};
