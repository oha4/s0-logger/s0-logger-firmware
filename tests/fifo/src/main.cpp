#include "gtest/gtest.h"
#include <nsi_main.h>

int main(void)
{
    testing::InitGoogleTest();
    int ret = RUN_ALL_TESTS();
    nsi_exit(ret); /* actually this will exit */
    return 0;
}