#include "gmock/gmock.h"
#include <fifo.h>

template <typename T, size_t N>
bool arraysEqual(const std::array<T, N> &array1, const std::array<T, N> &array2)
{
  return std::equal(array1.begin(), array1.end(), array2.begin());
}

MATCHER_P(ArrayContains, n, "")
{
  size_t size = (n.size() < arg.size()) ? n.size() : arg.size();
  for(int i=0; i < size; i++){
    if(arg[i] != n[i]) return false;
  }
  return true;
}

static const std::size_t BUF_SIZE = 10;

class FIFOBasic : public testing::Test
{
public:
  FIFOBasic() : mem(std::make_unique<uint8_t[]>(BUF_SIZE)), store(std::move(mem), BUF_SIZE) {}

protected:
  std::unique_ptr<uint8_t[]> mem;
  FIFO store;
};

class FIFOInvalid : public testing::Test
{
public:
  FIFOInvalid() : store(std::unique_ptr<uint8_t[]>(nullptr), 0) {}

protected:
  FIFO store;
};

TEST_F(FIFOBasic, GetSize)
{
  EXPECT_EQ(store.size(), BUF_SIZE);
}

TEST_F(FIFOBasic, EmptyAtTheBeginning)
{
  EXPECT_THAT(store.isEmpty(), testing::IsTrue());
}

TEST_F(FIFOBasic, NotEmptyAfterFirstPush)
{
  std::array<uint8_t, 3> data = {1, 2, 3};
  store.push(data.data(), data.size());

  EXPECT_THAT(store.isEmpty(), testing::IsFalse());
}

TEST_F(FIFOBasic, NotEmptyIfFull)
{
  std::array<uint8_t, 4> data = {1, 2, 3, 4};
  store.push(data.data(), data.size());
  store.push(data.data(), data.size());

  EXPECT_THAT(store.isEmpty(), testing::IsFalse());
}

TEST_F(FIFOBasic, SizeIsZeroAfterMoveConstructor)
{
  FIFO second(std::move(store));

  EXPECT_EQ(store.size(), 0);
}

TEST_F(FIFOBasic, SizeIsZeroAfterAssignementOperator)
{
  auto buf = std::make_unique<uint8_t[]>(10);
  FIFO second(std::move(buf), 10);
  second  = std::move(store);

  EXPECT_EQ(store.size(), 0);
}

TEST_F(FIFOBasic, DoesNotDeleteIfAssigningToItself)
{
  store = std::move(store);

  EXPECT_EQ(store.size(), 10);
}

TEST_F(FIFOBasic, InsertSingleElement)
{
  std::array<uint8_t, 3> data = {1, 2, 3};
  std::array<uint8_t, 3> readback = {};

  store.push(data.data(), data.size());
  store.pop(readback.data(), readback.size());

  EXPECT_THAT(readback, testing::ContainerEq(data));
}

TEST_F(FIFOBasic, InsertMultipleElements)
{
  std::array<uint8_t, 3> data = {1, 2, 3};
  std::array<uint8_t, 6> expected = {1, 2, 3, 1, 2, 3};
  std::array<uint8_t, 6> readback = {};

  store.push(data.data(), data.size());
  store.push(data.data(), data.size());
  store.pop(readback.data(), readback.size());

  EXPECT_THAT(readback, testing::ContainerEq(expected));
}

TEST_F(FIFOBasic, InsertMultipleElementsExceedingStoreSize)
{
  std::array<uint8_t, 3> data = {1, 2, 3};
  std::array<uint8_t, 3> data2 = {4, 5, 6};
  std::array<uint8_t, 10> expected = {1, 2, 3, 4, 5, 6};
  std::array<uint8_t, 10> readback = {};

  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());
  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());
  store.pop(readback.data(), readback.size());

  EXPECT_THAT(readback, testing::ContainerEq(expected));
}

TEST_F(FIFOBasic, PopReturnsCompleteElement)
{
  std::array<uint8_t, 3> data = {1, 2, 3};
  std::array<uint8_t, 3> data2 = {4, 5, 6};
  std::array<uint8_t, 10> readback = {};

  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());
  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());


  size_t size = store.pop(readback.data(), 5);

  EXPECT_THAT(size, 3);
  EXPECT_THAT(data, ArrayContains(readback));
}

TEST_F(FIFOBasic, PopReturnsCompleteMultipleElements)
{
  std::array<uint8_t, 3> data = {1, 2, 3};
  std::array<uint8_t, 3> data2 = {4, 5, 6};
  std::array<uint8_t, 10> readback1 = {};
  std::array<uint8_t, 10> readback2 = {};

  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());
  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());

  size_t size = store.pop(readback1.data(), 5);
  EXPECT_THAT(size, 3);
  EXPECT_THAT(data, ArrayContains(readback1));

  size = store.pop(readback2.data(), 5);
  EXPECT_THAT(size, 3);
  EXPECT_THAT(data2, ArrayContains(readback2));
}

TEST_F(FIFOBasic, PopReturnsCompleteMultipleElements2)
{
  std::array<uint8_t, 3> data = {1, 2, 3};
  std::array<uint8_t, 3> data2 = {4, 5, 6};
  std::array<uint8_t, 10> readback1 = {};

  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());
  store.push(data.data(), data.size());
  store.push(data2.data(), data.size());

  size_t size = store.pop(readback1.data(), 6);
  EXPECT_THAT(size, 6);
  std::array<uint8_t, 6> expected = {1,2,3,4,5,6};
  EXPECT_THAT(data, ArrayContains(expected));
}

TEST_F(FIFOBasic, PopOnEmptyFifoReturnsZeroBytes)
{
  std::array<uint8_t, 10> readback = {};
  size_t len = store.pop(readback.data(), readback.size());

  EXPECT_THAT(len, testing::Eq(0));
}

TEST_F(FIFOBasic, PopIntoNullPointerIsRejected)
{
  std::array<uint8_t, 10> readback = {};
  size_t len = store.pop(nullptr, readback.size());

  EXPECT_THAT(len, testing::Eq(0));
}

TEST_F(FIFOBasic, PopWithZeroSize)
{
  std::array<uint8_t, 5> data = {1,2,3,4,5};
  std::array<uint8_t, 10> readback = {};

  store.push(data.data(), data.size());
  size_t len = store.pop(readback.data(), 0);

  EXPECT_THAT(len, testing::Eq(0));
  EXPECT_THAT(data, testing::ElementsAreArray({1,2,3,4,5}));
}

TEST_F(FIFOBasic, PushWithNullpointerIsrejected)
{
  std::array<uint8_t, 5> data = {1, 2, 3, 4, 5};

  size_t len = store.push(nullptr, data.size());

  EXPECT_THAT(len, testing::Eq(0));
}

TEST_F(FIFOBasic, PushWithZeroSize)
{
  std::array<uint8_t, 5> data = {1, 2, 3, 4, 5};

  size_t len = store.push(data.data(), 0);

  EXPECT_THAT(len, testing::Eq(0));
  EXPECT_THAT(data, testing::ElementsAreArray({1, 2, 3, 4, 5}));
}

TEST_F(FIFOInvalid, PushIsRejected){
  std::array<uint8_t, 5> data = {1, 2, 3, 4, 5};

  size_t len = store.push(data.data(), 0);

  EXPECT_THAT(len, testing::Eq(0));
  EXPECT_THAT(data, testing::ElementsAreArray({1, 2, 3, 4, 5}));
}

TEST_F(FIFOInvalid, PopIsRejected)
{
  std::array<uint8_t, 5> data = {1, 2, 3, 4, 5};
  std::array<uint8_t, 5> data2 = {};

  store.push(data.data(), data.size());
  size_t len = store.pop(data.data(), data.size());

  EXPECT_THAT(len, testing::Eq(0));
  EXPECT_THAT(data2, testing::ElementsAreArray({0,0,0,0,0}));
}

TEST_F(FIFOInvalid, IsAlwaysEmpty)
{
  std::array<uint8_t, 5> data = {1, 2, 3, 4, 5};
  EXPECT_THAT(store.isEmpty(), testing::IsTrue());

  store.push(data.data(), data.size());
  EXPECT_THAT(store.isEmpty(), testing::IsTrue());

  store.pop(data.data(), data.size());
  EXPECT_THAT(store.isEmpty(), testing::IsTrue());

}

TEST_F(FIFOBasic, usedSpaceIszeroAtTheBeginning)
{
  EXPECT_THAT(store.usedSpace(), testing::Eq(0));
}

TEST_F(FIFOBasic, UsedSpaceAfterPush)
{
  std::array<uint8_t, 5> data = {1, 2, 3, 4, 5};
  store.push(data.data(), data.size());

  EXPECT_THAT(store.usedSpace(), testing::Eq(6));
}

TEST_F(FIFOBasic, FreeSpaceAtTheBeginning)
{
  EXPECT_THAT(store.freeSpace(), testing::Eq(BUF_SIZE));
}

TEST_F(FIFOBasic, FreeSpaceAfterPush)
{
  std::array<uint8_t, 5> data = {1, 2, 3, 4, 5};
  store.push(data.data(), data.size());

  EXPECT_THAT(store.freeSpace(), testing::Eq(BUF_SIZE - 6));
}