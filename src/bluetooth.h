/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include <zephyr/bluetooth/bluetooth.h>

#include <vector>
#include <initializer_list>

class BluetoothAdvertiser {
private:
    struct AdvertiseWorkData{
      struct k_work work;
      BluetoothAdvertiser* adv;
    };

    static AdvertiseWorkData advertise_work;

    static const struct bt_data ad[];
    const std::vector<struct bt_data> sd;

    static void advertise(struct k_work *work);
    static void connected(struct bt_conn *conn, uint8_t err);
    static void disconnected(struct bt_conn *conn, uint8_t reason);

    static void btReady(int err);
    
    BluetoothAdvertiser(const std::vector<struct bt_data> scan_response_data);

protected:
  std::vector<struct bt_data>& scan_response();

public:
    static BluetoothAdvertiser &instance(const std::vector<struct bt_data> scan_response_data);
    int start();
};

#endif /* BLUETOOTH_H */