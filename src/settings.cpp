/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "settings.h"

#include <zephyr/logging/log.h>
#include <zephyr/settings/settings.h>
#include <sys/errno.h>

#include <array>
#include <string_view>

LOG_MODULE_REGISTER(app_settings);

struct WifiSettings{
  char ssid[128];
  char password[128];
} settings = {};

int Settings::Wifi::load(){
  int ret = settings_subsys_init();
  if(ret != 0){
    return ret;
  }

  ret = settings_load_subtree("wifi");

  return ret;
}

const std::string_view Settings::Wifi::get_ssid(){
  return settings.ssid;
}

const std::string_view Settings::Wifi::get_password(){
  return settings.password;
}

void Settings::Wifi::set_ssid(std::string_view s){
  std::copy(std::begin(s), std::begin(s)+s.length(), settings.ssid);
}

void Settings::Wifi::set_password(std::string_view s){
  std::copy(std::begin(s), std::begin(s)+s.size(), settings.password);
}

void Settings::Wifi::save(){
  settings_save();
}

static int wifi_get(const char *key, char *val, int val_len_max){
  int len = -1;
  std::string_view s(key);
  if(s.compare("ssid") == 0){
    len = MIN(val_len_max, strlen(settings.ssid)+1);
    LOG_INF("Found setting \"ssid\" with value: %s, length: %d", settings.ssid, len);
    strncpy(val, settings.ssid, len);
  }else if(s.compare("password") == 0){
    len = MIN(val_len_max, strlen(settings.password)+1);
    LOG_INF("Found setting \"password\" with value: %s, length: %d", settings.password, len);
    strncpy(val, settings.password, len);
  }

  return len;
}

static int wifi_set(const char *name, size_t len, settings_read_cb read_cb, void *cb_arg){
  const char *next;
  size_t name_len;

  name_len = settings_name_next(name, &next);
  std::string_view s(name, name_len);

  if (!next) {
    int rc;
    if (!s.compare("ssid")){
      rc = read_cb(cb_arg, reinterpret_cast<void*>(settings.ssid), sizeof(settings.ssid));
      if(rc == 0){
        LOG_WRN("Could not find wifi/ssid setting");
      }
      LOG_INF("Written setting \"ssid\" with value %s", settings.ssid);
      return 0;
    }
    if (!s.compare("password")){
      rc = read_cb(cb_arg, reinterpret_cast<void*>(const_cast<char*>(settings.password)), sizeof(settings.password));
      if(rc == 0){
        LOG_WRN("Could not find wifi/password setting");
      }
      LOG_INF("Written setting \"password\" with value %s", settings.password);
      return 0;
    }
  }

  return -ENOENT;
}

static int wifi_export(int (*cb)(const char *name, const void *value, size_t val_len)){
	LOG_INF("export keys under <wifi> handler");
	cb("wifi/ssid", settings.ssid, std::string_view(settings.ssid).size());
	cb("wifi/password", settings.password, std::string_view(settings.password).size());

	return 0;
}

static int wifi_commit(void)
{
	LOG_INF("loading all settings under <wifi> handler is done");
	return 0;
}

int Settings::Wifi::delete_all(){
  int ret;
  ret = settings_delete("wifi/ssid");
  if(ret != 0){
    LOG_ERR("Could not delete setting `wifi/ssid`");
  }

  ret = settings_delete("wifi/password");
  if(ret != 0){
    LOG_ERR("Could not delete setting `wifi/password`");
  }
  return 0;
}

SETTINGS_STATIC_HANDLER_DEFINE(wifi_handler, "wifi", wifi_get, wifi_set, wifi_commit, wifi_export);

///////////////////////
// Settings::LWM2M
///////////////////////

template<typename T>
int read_data(std::string_view s, std::string_view expected, T val, size_t size, settings_read_cb read_cb, void *cb_arg){
      if (!s.compare(expected)){
        LOG_DBG("Read %s with %d bytes", s.data(), size);
        int rc = read_cb(cb_arg, reinterpret_cast<void*>(val), size);
        if(rc == 0){
          LOG_WRN("Could not find %s setting", expected.data());
        }
        return 0;
      }
      return 1;
    };


struct LWM2MSettings{
  char server[128];
  char manufacturer[128];
  char hardware_version[128];
  char device_type[128];
} lwm2m_settings = {};

int Settings::LWM2M::load(){
    int ret = settings_subsys_init();
  if(ret != 0){
    return ret;
  }

  ret = settings_load_subtree("lwm2m");

  return ret;
}

const std::string_view Settings::LWM2M::get_server(){
  return lwm2m_settings.server;
}

const std::string_view Settings::LWM2M::get_manufacturer(){
  return lwm2m_settings.manufacturer;
}

const std::string_view Settings::LWM2M::get_hardware_version(){
  return lwm2m_settings.hardware_version;
}

const std::string_view Settings::LWM2M::get_device_type(){
  return lwm2m_settings.device_type;
}

static int lwm2m_get(const char *key, char *val, int val_len_max){
  int len = -1;
  std::string_view s(key);
  if(s.compare("server") == 0){
    len = MIN(val_len_max, strlen(lwm2m_settings.server)+1);
    LOG_INF("Found setting \"server\" with value: %s, length: %d", lwm2m_settings.server, len);
    strncpy(val, lwm2m_settings.server, len);

  }else if(s.compare("manufacturer") == 0){
    len = MIN(val_len_max, strlen(lwm2m_settings.manufacturer)+1);
    LOG_INF("Found setting \"manufacturer\" with value: %s, length: %d", lwm2m_settings.manufacturer, len);
    strncpy(val, lwm2m_settings.manufacturer, len);

  }else if(s.compare("hardware_version") == 0){
    len = MIN(val_len_max, strlen(lwm2m_settings.hardware_version)+1);
    LOG_INF("Found setting \"hardware_version\" with value: %s, length: %d", lwm2m_settings.hardware_version, len);
    strncpy(val, lwm2m_settings.hardware_version, len);

  }else if(s.compare("device_type") == 0){
    len = MIN(val_len_max, strlen(lwm2m_settings.device_type)+1);
    LOG_INF("Found setting \"device_type\" with value: %s, length: %d", lwm2m_settings.device_type, len);
    strncpy(val, lwm2m_settings.device_type, len);
  }

  return len;
}

static int lwm2m_set(const char *name, size_t len, settings_read_cb read_cb, void *cb_arg){
  const char *next;
  size_t name_len;

  name_len = settings_name_next(name, &next);
  std::string_view s(name, name_len);

  if (!next) {
    if(read_data<char[128]>(s, "server", lwm2m_settings.server, len, read_cb, cb_arg) == 0){
      LOG_INF("Written setting \"server\" with value %s",lwm2m_settings.server);
      return 0;
    }
    if(read_data<char[128]>(s, "manufacturer", lwm2m_settings.manufacturer, len, read_cb, cb_arg) == 0){
      LOG_INF("Written value %s", lwm2m_settings.manufacturer);
      return 0;
    } 
    if(read_data<char[128]>(s, "hardware_version", lwm2m_settings.hardware_version, len, read_cb, cb_arg) == 0){
      LOG_INF("Written setting \"hardware_version\" with value %s", lwm2m_settings.hardware_version);
      return 0;
    } 
    if(read_data<char[128]>(s, "device_type", lwm2m_settings.device_type, len, read_cb, cb_arg) == 0){
      LOG_INF("Written setting \"device_type\" with value %s", lwm2m_settings.device_type);
      return 0;
    } 
  }

  return -ENOENT;
}

static int lwm2m_export(int (*cb)(const char *name, const void *value, size_t val_len)){
	LOG_INF("export keys under <lwm2m> handler");
	cb("lwm2m/server", lwm2m_settings.server, std::string_view(lwm2m_settings.server).size());
	cb("lwm2m/manufacturer", lwm2m_settings.manufacturer, std::string_view(lwm2m_settings.manufacturer).size());
	cb("lwm2m/hardware_version", lwm2m_settings.hardware_version, std::string_view(lwm2m_settings.hardware_version).size());

	return 0;
}

static int lwm2m_commit(void)
{
	LOG_INF("loading all settings under <lwm2m> handler is done");
	return 0;
}

int Settings::LWM2M::delete_all(){
  int ret = 0;

  ret |= delete_server();
  ret |= delete_manufacturer();
  ret |= delete_hardware_version();
  ret |= delete_device_type();

  return 0;
}

int Settings::LWM2M::delete_server(){
  int ret;
  ret = settings_delete("lwm2m/server");
  if(ret != 0){
    LOG_ERR("Could not delete setting `lwm2m/server`");
  }

  return ret;
}

int Settings::LWM2M::delete_manufacturer(){
  int ret;
  ret = settings_delete("lwm2m/manufacturer");
  if(ret != 0){
    LOG_ERR("Could not delete setting `lwm2m/manufacturer`");
  }

  return ret;
}

int Settings::LWM2M::delete_hardware_version(){
  int ret;
  ret = settings_delete("lwm2m/hardware_version");
  if(ret != 0){
    LOG_ERR("Could not delete setting `lwm2m/hardware_version`");
  }

  return ret;
}

int Settings::LWM2M::delete_device_type(){
  int ret;
  ret = settings_delete("lwm2m/device_type");
  if(ret != 0){
    LOG_ERR("Could not delete setting `lwm2m/device_type`");
  }

  return ret;
}

SETTINGS_STATIC_HANDLER_DEFINE(lwm2m_handler, "lwm2m", lwm2m_get, lwm2m_set, lwm2m_commit, lwm2m_export);


///////////////////////
// Settings::TECH
///////////////////////

struct TechSettings{
  uint32_t imp_perkwh;
} tech_settings = {};

int Settings::TECH::load(){
    int ret = settings_subsys_init();
  if(ret != 0){
    return ret;
  }

  ret = settings_load_subtree("tech");

  return ret;
}

const uint32_t Settings::TECH::get_imp_per_kwh(){
  return tech_settings.imp_perkwh;
}

static int tech_get(const char *key, char *val, int val_len_max){
  int len = -1;
  std::string_view s(key);
  if(s.compare("imp_per_kwh") == 0){
    len = MIN(val_len_max, sizeof(tech_settings.imp_perkwh));
    LOG_INF("Found setting \"imp_per_kwh\" with value: %d, length: %d", tech_settings.imp_perkwh, len);
    memcpy(val, &tech_settings.imp_perkwh, len);
  }

  return len;
}

static int tech_set(const char *name, size_t len, settings_read_cb read_cb, void *cb_arg){
  const char *next;
  size_t name_len;

  name_len = settings_name_next(name, &next);
  std::string_view s(name, name_len);

  if (!next) {
    if(read_data(s, "imp_per_kwh", &tech_settings.imp_perkwh, len, read_cb, cb_arg) == 0){
      LOG_INF("Written setting \"imp_per_kwh\" with value %d", tech_settings.imp_perkwh);
      return 0;
    }
  }

  return -ENOENT;
}

static int tech_export(int (*cb)(const char *name, const void *value, size_t val_len)){
	LOG_INF("export keys under <tech> handler");
	cb("tech/imp_per_kwh", &tech_settings.imp_perkwh, sizeof(tech_settings.imp_perkwh));

	return 0;
}

static int tech_commit(void)
{
	LOG_INF("loading all settings under <tech> handler is done");
	return 0;
}

SETTINGS_STATIC_HANDLER_DEFINE(tech_handler, "tech", tech_get, tech_set, tech_commit, tech_export);