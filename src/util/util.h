/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef UTIL_H_
#define UTIL_H_

/**
 * @brief Makes the inherited class noncopyable
 * 
 */
class Noncopyable{
protected:
  Noncopyable() {}
  ~Noncopyable() {}

private:
  Noncopyable(const Noncopyable&) = delete;
  const Noncopyable& operator=( const Noncopyable& ); // non copyable
};

#endif /* UTIL_H_ */