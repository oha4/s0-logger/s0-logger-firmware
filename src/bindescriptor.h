/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <zephyr/bindesc.h>

#define BINDESC_ID_MODULE_TYPE 0x0000

BINDESC_STR_DEFINE(module_type, BINDESC_ID_MODULE_TYPE, "S0-Logger");