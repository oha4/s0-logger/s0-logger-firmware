/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "bluetooth.h"
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/mgmt/mcumgr/transport/smp_bt.h>

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(bluetooth);

// Initialisierung der statischen Mitglieder
const struct bt_data BluetoothAdvertiser::ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA_BYTES(BT_DATA_UUID128_ALL,
                  0x84, 0xaa, 0x60, 0x74, 0x52, 0x8a, 0x8b, 0x86,
                  0xd3, 0x4c, 0xb7, 0x1d, 0x1d, 0xdc, 0x53, 0x8d),
};

BluetoothAdvertiser::AdvertiseWorkData BluetoothAdvertiser::advertise_work;

BluetoothAdvertiser& BluetoothAdvertiser::instance(const std::vector<struct bt_data> scan_response_data)
{
    static BluetoothAdvertiser instance(scan_response_data);
    return instance;
}

int BluetoothAdvertiser::start()
{
    int rc;
    rc = bt_enable(&BluetoothAdvertiser::btReady);
    if (rc != 0) {
        LOG_ERR("Bluetooth enable failed: %d", rc);
    }

    return rc;
}

// BluetoothAdvertiser::BluetoothAdvertiser() : sd{BT_DATA(BT_DATA_NAME_COMPLETE, "S0-Logger", strlen("S0-Logger"))}{
BluetoothAdvertiser::BluetoothAdvertiser(const std::vector<struct bt_data> scan_response_data) : sd{scan_response_data}{
  advertise_work.adv = this;
  k_work_init(&advertise_work.work, advertise);
}

void BluetoothAdvertiser::advertise(struct k_work *work)
{
    int rc;
    bt_le_adv_stop();

    struct bt_le_adv_param param BT_LE_ADV_PARAM_INIT(BT_LE_ADV_OPT_CONNECTABLE, BT_GAP_ADV_FAST_INT_MIN_2, BT_GAP_ADV_FAST_INT_MAX_2, NULL);

    rc = bt_le_adv_start(&param, ad, ARRAY_SIZE(ad), advertise_work.adv->sd.data(), advertise_work.adv->sd.size());
    if (rc) {
        LOG_ERR("Advertising failed to start (rc %d)", rc);
        return;
    }
    LOG_INF("Advertising successfully started");
}

void BluetoothAdvertiser::connected(struct bt_conn *conn, uint8_t err)
{
    if (err) {
        LOG_ERR("Connection failed (err 0x%02x)", err);
    } else {
        LOG_INF("Connected");
    }
}

void BluetoothAdvertiser::disconnected(struct bt_conn *conn, uint8_t reason)
{
    LOG_INF("Disconnected (reason 0x%02x)", reason);
    k_work_submit(&advertise_work.work);
}

void BluetoothAdvertiser::btReady(int err)
{
    if (err != 0) {
        LOG_ERR("Bluetooth failed to initialise: %d", err);
    } else {
        k_work_submit(&advertise_work.work);
    }
}