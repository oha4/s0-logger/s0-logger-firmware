/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#include <cstddef>
#include <string>

namespace Settings{
namespace Wifi {

  int load();
  int delete_all();
  const std::string_view get_ssid();
  void set_ssid(std::string_view s);
  
  const std::string_view get_password();
  void set_password(std::string_view s);

  void save();
}; /* Wifi */

namespace LWM2M {
  int load();
  int delete_all();
  int delete_server();
  int delete_hardware_version();
  int delete_manufacturer();
  int delete_device_type();
  const std::string_view get_server();
  const std::string_view get_hardware_version();
  const std::string_view get_manufacturer();
  const std::string_view get_device_type();
  int version();
};

namespace TECH {
  int load();
  const uint32_t get_imp_per_kwh();
};
}; /* Settings */

#endif /* _SETTINGS_H_ */