/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _HW_CFG_H_
#define _HW_CFG_H_

#include <gpio.h>
#include <timer.h>
#include <factoryreset.h>
#include <uniqueid.h>
#include <wdt.h>
#include <frequency.h>

using APP_GPIO = ZephyrGPIO;
using ResetFactory = FactoryReset<ZephyrGPIOInterrupt>;
using APP_UniqueID = ZephyrUniqueID;
using APP_Timer = ZephyrTimer;
using APP_Watchdog = ZephyrWatchdog;
using APP_Frequency = ZephyrFrequency;

#endif /* _HW_CFG_H_ */