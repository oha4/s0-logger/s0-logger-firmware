/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/storage/flash_map.h>
#include <zephyr/settings/settings.h>
#include <zephyr/sys/reboot.h>

LOG_MODULE_REGISTER(main);

#include <hw_cfg.h>
#include "settings.h"
#include "bindescriptor.h"
#include "fifo.h"
#include <application.h>
#include <factoryreset.h>
#include <errorhandler.h>
#include "tech.h"
#include <fwup.h>
#include <gpio.h>
#include <timer.h>
#include <logger.h>
#include <uniqueid.h>
#include <wdt.h>
#include <frequency.h>
#include <reset.h>

#include "wifi.h"
#include "bluetooth.h"
#include <ulwm2m.h>

#include <cstring>

static void reset_factory_timer_cb(){
  LOG_INF("Execute Reset to factory");
  Settings::Wifi::delete_all();
  Settings::LWM2M::delete_server();
  ZephyrReset::reboot();
}

static void reboot_timer_cb()
{
  LOG_INF("Execute device reboot");
  ZephyrReset::reboot();
}

int main(void)
{

  Wifi wifi;
  APP_GPIO error_led(GPIO_DT_SPEC_GET(DT_ALIAS(lederr), gpios));
  error_led.init();
  APP_GPIO con_led(GPIO_DT_SPEC_GET(DT_ALIAS(ledcon), gpios));
  ZephyrGPIOInterrupt btn_reset_factory(GPIO_DT_SPEC_GET(DT_ALIAS(factoryreset), gpios));
  ResetFactory reset_factory(btn_reset_factory);
  APP_UniqueID uid;
  ErrorHandler err(error_led);
  APP_Watchdog watchdog(DEVICE_DT_GET(DT_ALIAS(watchdog0)));
  ZephyrFrequency s0_input_0(GPIO_DT_SPEC_GET(DT_NODELABEL(input0), gpios), LWM2M_POWER_OBJECT_ID, 0, LWM2M_POWER_SENSOR_VALUE_ID);
  ZephyrFrequency s0_input_1(GPIO_DT_SPEC_GET(DT_NODELABEL(input1), gpios), LWM2M_POWER_OBJECT_ID, 1, LWM2M_POWER_SENSOR_VALUE_ID);

  std::string_view tmp("S0-Logger-");

  // Char-Array für den resultierenden C-String erstellen
  char dev_name[27] = {};
  std::memcpy(dev_name, tmp.data(), tmp.size());
  std::memcpy(dev_name + tmp.size(), uid.get_string().data(), uid.get_string().size()); // +1 für das Nullterminierungszeichen

  reset_factory.addCallback(10*1000, Callback0<void>::from_function<&reset_factory_timer_cb>());
  reset_factory.addCallback(2*1000, Callback0<void>::from_function<&reboot_timer_cb>());

  LOG_INFO("=====================================================");
  LOG_INFO("|            %s                            ", dev_name);
  LOG_INFO("|             " VERSION_STR "                        ");
  LOG_INFO("|                                                    ");
  LOG_INFO("==================================================== ");
  
  /* wait until network stack is up*/
  while(!wifi.is_up()){
    k_msleep(100);
  }

  /* start bluetooth very early to have a fallback connection */
  BluetoothAdvertiser& bt = BluetoothAdvertiser::instance({BT_DATA(BT_DATA_NAME_COMPLETE, dev_name, strlen(dev_name))});
  bt.start();

  /**
   * Load settings from NVS
   */
  Settings::Wifi::load();
  Settings::LWM2M::load();
  Settings::TECH::load();

  if(Settings::Wifi::get_ssid().empty() ||
      Settings::Wifi::get_password().empty() ||
      Settings::LWM2M::get_server().empty() ||
      Settings::LWM2M::get_manufacturer().empty() ||
      Settings::LWM2M::get_hardware_version().empty() ||
      (Settings::TECH::get_imp_per_kwh() == 0)){
    LOG_INFO("Could not start application. We need all parameters!");
    err.set(Errors::PARAMETER);

    while(1){
      k_msleep(100);
    }
  }

  LOG_INFO("Start Scan for SSID's");
  auto ssids = wifi.scan();
  if(ssids.empty()){
    LOG_ERROR("Could not find any SSID's");
    err.set(Errors::WIFI);
    return -1;
  }
  for(auto it=ssids.begin(); it!=ssids.end(); it++){
    LOG_INFO("Found ssid %s", it->c_str());
  }

  wifi.connect(Settings::Wifi::get_ssid(), Settings::Wifi::get_password());
  if(wifi.wait_for_connected(10000) == true)
  {
    LOG_INFO("Successfully connected to %s", Settings::Wifi::get_ssid().data());
  }else{
    LOG_ERROR("Could not connect to %s", Settings::Wifi::get_ssid().data());
    err.set(Errors::WIFI);
    return -1;
  }

  if(wifi.wait_for_dhcp_address()){
    LOG_INFO("Successfully recevied IP address via DHCP");
    LOG_INFO("Device IP address: %s", wifi.get_ip().c_str());
  }else{
    LOG_ERROR("Did not receive IP address via DHCP");
    err.set(Errors::WIFI);
    return -1;
  }

  lwm2m_object_t obj_array[6] = {};
  LWM2MSettings lwm2m_settings{
    .server_address = Settings::LWM2M::get_server(),
    .manufacturer = Settings::LWM2M::get_manufacturer(),
    .device_type = Settings::LWM2M::get_device_type(),
    .hardware_version = Settings::LWM2M::get_hardware_version()
  };

  FirmwareUpdate fwup{
    .write_firmware = write_firmware,
    .execute_update = execute_update
  };

  Tech t(obj_array, sizeof(obj_array) / sizeof(lwm2m_object_t), lwm2m_settings, fwup, error_led, con_led, reset_factory, uid, Settings::TECH::get_imp_per_kwh(), watchdog, s0_input_0, s0_input_1);

  t.init();
  t.start();

  return 0;
}
