/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "tech.h"

#ifndef SKIP_LOGGER
#include <logger.h>

LOG_MODULE_REGISTER(tech);
#endif

Tech* Tech::m_tech_ptr = nullptr;


float Tech::s0_0_callback(void){
  // Measurement is done every 5 Minutes. So we scale the value up to 1 hour by multiplying by 12. We also need to multiply by 1000 because measurement Unit is kWh.
  float val = m_tech_ptr->m_s0_input_0.get() * 12000 / m_tech_ptr->m_imp_per_kwh;
  LOG_INF("Reading Power of S0-Input 0. Value: %f", val);
  return val;
}

float Tech::s0_1_callback(void){
  // Measurement is done every 5 Minutes. So we scale the value up to 1 hour by multiplying by 12. We also need to multiply by 1000 because measurement Unit is kWh.
  float val = m_tech_ptr->m_s0_input_0.get() * 12000 / m_tech_ptr->m_imp_per_kwh;
  LOG_INF("Reading Power of S0-Input 1. Value: %f", val);
  return val;
}

Tech::Tech(lwm2m_object_t *obj_array, std::size_t m_num_obj, const LWM2MSettings lwm2m_settings, FirmwareUpdate fwup, APP_GPIO &err_led, APP_GPIO &con_led, ResetFactory &reset_factory, APP_UniqueID &unique_id, const uint32_t imp_per_kwh, APP_Watchdog &watchdog, APP_Frequency &s0_input_0, APP_Frequency &s0_input_1):
  Application(obj_array, m_num_obj, lwm2m_settings, fwup, err_led, con_led, reset_factory, unique_id, watchdog),
  m_s0_input_0(s0_input_0),
  m_s0_input_1(s0_input_1),
  m_power_data{ .power=s0_0_callback, .units="Watt"},
  m_power_data2{ .power=s0_1_callback, .units="Watt"},
  m_imp_per_kwh(imp_per_kwh){
    m_tech_ptr = this;
}

int Tech::tech_init(lwm2m_object_t * obj_array, const std::size_t max_num_obj){
  int ret = 0;

  LOG_INF("Tech Init()");

  // S0-input signals
  ret = m_s0_input_0.init(300*1000);
  if(ret < 0){
    LOG_ERR("Could not initialize S0-Input 0");
    return -1;
  }
  ret = m_s0_input_1.init(300 * 1000);
  if(ret < 0){
    LOG_ERR("Could not initialize S0-Input 1");
    return -1;
  }

  /* ------------------------------------ */
  /* Callbacks for S0-Input Value changes */
  /* ------------------------------------ */
  m_s0_input_0.registerObserver(&this->m_observer);
  m_s0_input_1.registerObserver(&this->m_observer);

  ret = lwm2m_init_object(&obj_array[0], LWM2M_POWER_OBJECT_ID, lwm2m_power_read, lwm2m_power_write, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERR("Failed to create Frequency object\r\n");
    return -1;
  }

  /* --------------------------- */
  /* create frequency instance 0 */
  /* --------------------------- */
  lwm2m_instance_t * freq = lwm2m_create_instance(0, &m_power_data, NULL, false);
  lwm2m_object_add_instance(&obj_array[0], freq);

  /* --------------------------- */
  /* create frequency instance 1 */
  /* --------------------------- */
  lwm2m_instance_t * freq2 = lwm2m_create_instance(1, &m_power_data2, NULL, false);
  lwm2m_object_add_instance(&obj_array[0], freq2);

  return 0;
}

int Tech::tech_stop(){
  LOG_INFO("Tech Stop()");
  return 0;
}

int Tech::tech_start(){
  LOG_INFO("Tech Start()");
  return 0;
}