/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "wifi.h"

#include <zephyr/net/wifi.h>
#include <zephyr/net/net_if.h>
#include <zephyr/net/net_event.h>
#include <zephyr/kernel.h>

LOG_MODULE_REGISTER(wifi);

#define WIFI_MGMT_EVENTS (  NET_EVENT_WIFI_SCAN_RESULT | \
                            NET_EVENT_WIFI_SCAN_DONE | \
                            NET_EVENT_WIFI_CONNECT_RESULT |		\
				            NET_EVENT_WIFI_DISCONNECT_RESULT)

#define NET_MGMT_EVENTS ( NET_EVENT_IPV4_ADDR_ADD)

enum WifiState{
    SCANNING,
    CONNECTED,
    DISCONNECTED,
    DHCP_ADDRESS_RECEIVED
};
static WifiState m_state = WifiState::DISCONNECTED;
static bool m_received_ip_address = false;
static k_event m_ev;

static std::vector<wifi_scan_result> m_scan_results = {};

/*
 * Forward Declaration
 */

void event_listener( struct net_mgmt_event_callback *cb, uint32_t mgmt_event, struct net_if *iface );
void net_event_listener( struct net_mgmt_event_callback *cb, uint32_t mgmt_event, struct net_if *iface );

Wifi::Wifi()
{
    net_mgmt_init_event_callback(&m_cb, event_listener, WIFI_MGMT_EVENTS );
	net_mgmt_add_event_callback(&m_cb);
    net_mgmt_init_event_callback(&m_net_cb, net_event_listener, NET_MGMT_EVENTS );
	net_mgmt_add_event_callback(&m_net_cb);

    k_event_init(&m_ev);
}

bool Wifi::is_up()
{
    struct net_if *iface = net_if_get_default();
    return net_if_flag_is_set(iface, NET_IF_LOWER_UP);
}

std::vector<std::string> Wifi::scan()
{
    m_scan_results.clear();
    struct net_if *iface = net_if_get_default();
    if(net_mgmt(NET_REQUEST_WIFI_SCAN, iface, nullptr, 0))
    {
        LOG_ERR("Scan request failed");
        return {};
    }

    LOG_INF("Scan requested");
    m_state = WifiState::SCANNING;
    int ret = net_mgmt_event_wait(NET_EVENT_WIFI_SCAN_DONE, nullptr, &iface, nullptr, 0, Z_TIMEOUT_MS(10000));
    if(ret == ETIMEDOUT)
    {
        LOG_ERR("Timeout during Scan");
    }

    std::vector<std::string> ssids;
    ssids.reserve(m_scan_results.size());
    for(auto it=m_scan_results.begin(); it!=m_scan_results.end();it++)
    {
        ssids.push_back(reinterpret_cast<char*>(it->ssid));
    }

    return ssids;
}

void Wifi::connect(const std::string_view& ssid, const std::string_view& pw)
{
    struct wifi_connect_req_params wifi_args;
	wifi_args.security = WIFI_SECURITY_TYPE_PSK;
	wifi_args.channel = WIFI_CHANNEL_ANY;
	wifi_args.ssid = reinterpret_cast<const uint8_t*>(ssid.data());
	wifi_args.ssid_length = ssid.size();
    if(!pw.empty())
    {
        wifi_args.psk = reinterpret_cast<uint8_t*>(const_cast<char*>(pw.data()));
        wifi_args.psk_length = pw.size();
        wifi_args.security = WIFI_SECURITY_TYPE_PSK;
        wifi_args.mfp = WIFI_MFP_OPTIONAL;
    }else{
        wifi_args.security = WIFI_SECURITY_TYPE_NONE;
    }
    // wifi_args.timeout = 5 * MSEC_PER_SEC;

    // Connect interface to network
	struct net_if *iface = net_if_get_default();
	if( net_mgmt( NET_REQUEST_WIFI_CONNECT, iface, &wifi_args, sizeof(wifi_connect_req_params) ) ) {
		perror("Failed to request connection to SSID");
	}
}

bool Wifi::is_connected()
{
    if(m_state == WifiState::CONNECTED)
    {
        return true;
    }

    return false;
}

bool Wifi::wait_for_connected(uint32_t timeout_ms)
{
    uint32_t events = k_event_wait(&m_ev, WifiState::CONNECTED, true, Z_TIMEOUT_MS(timeout_ms));

    if(events == 0)
    {
        return false;
    }

    return true;
}

void event_listener( struct net_mgmt_event_callback *cb, uint32_t mgmt_event, struct net_if *iface )
{
	switch( mgmt_event ) {
		case NET_EVENT_WIFI_CONNECT_RESULT:
        {
			const struct wifi_status *status = (const struct wifi_status *) cb->info;
	        if (!status->status) {
		        // Connected
                LOG_INF("Successfully connected to wifi");
		        m_state = WifiState::CONNECTED;
	        }else {
                LOG_INF("Failed to connect to wifi");
                m_state = WifiState::DISCONNECTED;
            }
            
            k_event_post(&m_ev, m_state);
			break;
        }

        case NET_EVENT_WIFI_SCAN_DONE:
        {
            LOG_INF("Scan done");
            break;
        }

        case NET_EVENT_WIFI_SCAN_RESULT:
        {
            const wifi_scan_result * result = reinterpret_cast<const wifi_scan_result*>(cb->info);
            LOG_INF("Found WIFI SSID %s", result->ssid);
            m_scan_results.push_back(*result);
            break;
        }
	}
}

void net_event_listener( struct net_mgmt_event_callback *cb, uint32_t mgmt_event, struct net_if *iface ){
    switch( mgmt_event ) {
        case NET_EVENT_IPV4_ADDR_ADD:
        {
            LOG_INF("WIFI Network received new IP address");
            m_received_ip_address = true;
            k_event_post(&m_ev, DHCP_ADDRESS_RECEIVED);
            break;
        }
	}
}

void Wifi::disablePowerSafe(void){
    wifi_ps_params params = {WIFI_PS_DISABLED};
    struct net_if *iface = net_if_get_default();
	net_mgmt(NET_REQUEST_WIFI_PS, iface, &params, sizeof(params));
}

void Wifi::enablePowerSafe(void){
    wifi_ps_params params = {WIFI_PS_ENABLED};
    struct net_if *iface = net_if_get_default();
	net_mgmt(NET_REQUEST_WIFI_PS, iface, &params, sizeof(params));
}

void Wifi::request_dhcp_address(void){
    struct net_if *iface = net_if_get_default();
    net_dhcpv4_start(iface);
}

bool Wifi::wait_for_dhcp_address(uint32_t timeout_ms){
        uint32_t events = k_event_wait(&m_ev, WifiState::DHCP_ADDRESS_RECEIVED, true, Z_TIMEOUT_MS(timeout_ms));

    if(events == 0)
    {
        return false;
    }

    return true;
}

bool Wifi::got_dhcp_address(void){
    return m_received_ip_address;
}

std::string Wifi::get_ip(void){
    char buffer[16] = {};
    struct net_if *iface = net_if_get_default();
    struct net_if_config* cfg = net_if_get_config(iface);
    net_addr_ntop(AF_INET, &cfg->ip.ipv4->unicast->address.in_addr, buffer, sizeof(buffer));
    return buffer;
}

int Wifi::setup_accesspoint(const std::string_view& ssid, const std::string_view& password){
    struct wifi_connect_req_params  params;
    params.ssid = reinterpret_cast<const uint8_t*>(ssid.data());
    params.ssid_length = ssid.size();
    params.channel = 5;
    params.security = WIFI_SECURITY_TYPE_NONE;
    params.psk = reinterpret_cast<uint8_t*>(const_cast<char*>(password.data()));
    params.psk_length = password.size();
    params.mfp = WIFI_MFP_OPTIONAL;

    struct net_if *iface = net_if_get_default();
    if (net_mgmt(NET_REQUEST_WIFI_AP_ENABLE, iface, &params, sizeof(struct wifi_connect_req_params))) {
        LOG_ERR("Could not create WIFI access point");
        return -1;
    }
    LOG_INF("AP mode enabled\n");

    return 0;
}