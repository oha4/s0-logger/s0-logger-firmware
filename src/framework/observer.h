/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _OBSERVER_H_
#define _OBSERVER_H_

#include <vector>
#include <ulwm2m.h>

class IObserver
{
public:
  virtual ~IObserver() = default;
  virtual void onNotify(unsigned int object_id, unsigned int instance_id, unsigned int resource_id) = 0;
};

class IObservableBase
{
public:
  virtual ~IObservableBase() = default;

  void registerObserver(IObserver* observer);
  void removeObserver(IObserver* observer);
  void notifyObservers();

protected:
  IObservableBase() = delete;
  IObservableBase(unsigned int object_id, unsigned int instance_id, unsigned int resource_id);

private:
  std::vector<IObserver *> m_observers;
  unsigned int m_object_id;
  unsigned int m_instance_id;
  unsigned int m_resource_id;
};

class Lwm2mObserver : public IObserver
{
public:
  Lwm2mObserver() = default;

  void setContext(lwm2m_context_t* ctx);
  void onNotify(unsigned int object_id, unsigned int instance_id, unsigned int resource_id) override;

private:
  lwm2m_context_t* m_lwm2m_context = nullptr;
};

#endif /* _OBSERVER_H_ */