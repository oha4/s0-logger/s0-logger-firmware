/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef FIFO_H_
#define FIFO_H_

#include <cstdint>
#include <memory>



class FIFO
{
public:
    FIFO(std::unique_ptr<uint8_t[]> buffer, size_t size);
    FIFO(const FIFO &other) = delete;            // Disallow copy constructor
    FIFO &operator=(const FIFO &other) = delete; // Disallow copy assignment
    FIFO(FIFO &&other) noexcept;                 // Move constructor
    FIFO &operator=(FIFO &&other) noexcept;      // Move assignment operator

    // Adds an element of arbitrary size to the FIFO buffer.
    size_t push(const uint8_t *data, size_t length);

    // Reads elements from the FIFO buffer without removing them.
    size_t pop(uint8_t *dest, size_t maxLength);

    bool isEmpty() const;

    // Returns the number of bytes currently used in the buffer.
    size_t usedSpace() const;

    // Returns the number of bytes available in the buffer.
    size_t freeSpace() const;

    size_t size() const;

private:
    std::unique_ptr<uint8_t[]> buffer_;
    size_t size_;
    size_t head_;
    size_t tail_;
    bool empty_;
    bool valid_;

    // Discards elements to make space for new ones.
    void discardElements(size_t requiredSpace);
};

#endif /* FIFO_H_ */