/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "observer.h"

#include <algorithm>
#include <zephyr/logging/log.h>
#include <zephyr/kernel.h>

LOG_MODULE_REGISTER(Observer);

void IObservableBase::registerObserver(IObserver *observer){
  m_observers.push_back(observer);
}

void IObservableBase::removeObserver(IObserver *observer){
  m_observers.erase(std::remove(m_observers.begin(), m_observers.end(), observer), m_observers.end());
}

#pragma GCC optimize("O0")
void IObservableBase::notifyObservers(){
  for (auto *observer : m_observers)
  {
    observer->onNotify(m_object_id, m_instance_id, m_resource_id);
  }
}

IObservableBase::IObservableBase(unsigned int object_id, unsigned int instance_id, unsigned int resource_id): m_object_id(object_id), m_instance_id(instance_id), m_resource_id(resource_id){

}

void Lwm2mObserver::setContext(lwm2m_context_t* ctx){
  this->m_lwm2m_context = ctx;
}

void Lwm2mObserver::onNotify(unsigned int object_id, unsigned int instance_id, unsigned int resource_id){
  // printk("LWM2M Observe 0x%X: Notify %d/%d/%d about updated value\n", this->m_lwm2m_context, object_id, instance_id, resource_id);
  lwm2m_uri_t uri = {.flag = LWM2M_URI_FLAG_OBJECT_ID | LWM2M_URI_FLAG_INSTANCE_ID | LWM2M_URI_FLAG_RESOURCE_ID, .objectId = object_id, .instanceId = instance_id, .resourceId = resource_id};
  if (m_lwm2m_context != nullptr) lwm2m_resource_value_changed(m_lwm2m_context, &uri);
}