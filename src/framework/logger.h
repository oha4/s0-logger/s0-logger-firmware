/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _LOGGER_H_
#define _LOGGER_H_

#if defined(__ZEPHYR__)
#include <zephyr/logging/log.h>
#define LOG_WARNING(...) LOG_WRN(__VA_ARGS__)
#define LOG_INFO(...) LOG_INF(__VA_ARGS__)
#define LOG_ERROR(...) LOG_ERR(__VA_ARGS__)
#else
#define LOG_WARNING(...)
#define LOG_INFO(...)
#define LOG_ERROR(...)
#endif

#endif /* _LOGGER_H_ */