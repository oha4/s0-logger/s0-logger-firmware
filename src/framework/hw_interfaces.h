/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _HW_INTERFACES_H_
#define _HW_INTERFACES_H_

#include "callback.h"
#include <cstdint>
#include <string_view>
#include <array>
#include <functional>

/**
 * @brief Static Interface class for GPIO's
 * 
 * Uses CRTP mechanism to implement a static interface instead of a dynamic interface. Therefor not requiring a vtable.
 * 
 * @tparam T 
 */
template <typename T>
class GPIO{
public:

  enum State : uint32_t{
    ON,
    OFF
  };

  /**
   * @brief Returns the Current state of the LED
   * 
   * @return State 
   */
  State getState(){
    return static_cast<T&>(*this).getState();
  }

  /**
   * @brief Activate GPIO
   * 
   * @return Retuns 0 on success, -1 otherwise.
   */
  int set(){
    return static_cast<T&>(*this).set();
  }

    /**
   * @brief Deactivate GPIO
   * 
   * @return Returns 0 on Success, -1 otherwise
   */
  int clear(){
    return static_cast<T&>(*this).clear();
  }
};

template <typename T>
class GPIOInverted : public GPIO<T>{
public:

  /**
   * @brief Activate GPIO
   * 
   * @return Retuns 0 on success, -1 otherwise.
   */
  int set(){
    return static_cast<T&>(*this).clear();
  }

    /**
   * @brief Deactivate GPIO
   * 
   * @return Returns 0 on Success, -1 otherwise
   */
  int clear(){
    return static_cast<T&>(*this).set();
  }
};

template<typename T>
class GPIO_I{
public:
  bool get(){
    return static_cast<T*>(this)->get();
  }
};

template<typename T>
class GPIOInterrupt{
public:
  enum Edge {
    POSITIVE,
    NEGATIVE,
    BOTH
  };
  int init(Callback2<void, int, bool> cb, Edge edge){
    return static_cast<T*>(this)->init(cb, edge);
  }
};

/**
 * @brief Static Interface class to measure Frequency at an input Pin
 * 
 * @tparam T 
 */
template<typename T>
class Frequency{
public:

  /**
   * @brief Get last measured frequency in Hz
   * 
   * @return Frequency value in Hz 
   */
  float get(void){
    return static_cast<T&>(*this).get();
  }

  /**
   * @brief Definition of callback to use for `set_callback`
   * 
   */
  typedef std::function<void(float)> callback;
};

template<typename T>
class Timer{
public:
  int init(){
    return static_cast<T*>(this)->init();
  }

  void start(uint32_t seconds){
    static_cast<T*>(this)->start(seconds);
  }

  void stop(void){
    static_cast<T*>(this)->stop();
  }
};

template<typename T>
class UniqueID{
public:
  std::string_view get_string() const{
    return static_cast<T*>(this)->get_string();
  }

  std::array<uint8_t, 8> get() const{
    return static_cast<T*>(this)->get();
  }
};

/**
 * @brief CRTP (Curiously Recurring Template Pattern) class for Watchdog control.
 * 
 * @tparam T The derived class.
 */
template<typename T>
class Watchdog{
public:

  /**
   * @brief Triggers the Watchdog.
   */
  void trigger() const{
    static_cast<T*>(this)->trigger();
  }

  /**
   * @brief Initializes the Watchdog.
   * 
   * @param timeout Timeout in milliseconds. Defaults to 10 seconds
   */
  void init(const uint32_t timeout=10000) const{
    static_cast<T*>(this)->init(timeout);
  }
};

#endif /* _HW_INTERFACES_H_ */