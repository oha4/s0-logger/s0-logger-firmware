/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include <hw_cfg.h>
#include "errorhandler.h"
#include "factoryreset.h"
#include "callback.h"
#include "observer.h"
#include "nodelog.h"

#include <ulwm2m.h>

#include <string_view>
#include <string>
#include <functional>

struct LWM2MSettings{
  std::string_view server_address;
  std::string_view manufacturer;
  std::string_view device_type;
  std::string_view hardware_version;
};

struct FirmwareUpdate{
  uint32_t (*write_firmware)(uint8_t * buffer, int length, uint32_t block_num, uint8_t block_more);
  uint32_t (*execute_update)(void);
};

class Application{
public:
  Application(lwm2m_object_t *obj_array, std::size_t num_obj, const LWM2MSettings &lwm2m_settings, FirmwareUpdate fwup, APP_GPIO &err_led, APP_GPIO &con_led, ResetFactory &reset_factory, APP_UniqueID &unique_id, APP_Watchdog &watchdog);

  virtual ~Application(){};


  int init();
  int start();
  int stop();

protected:
  virtual int tech_init(lwm2m_object_t * obj_array, const std::size_t obj_offset) = 0;
  virtual int tech_start() = 0;
  virtual int tech_stop() = 0;

  Lwm2mObserver m_observer;

private:
  lwm2m_context_t * m_lwm2mH;
  lwm2m_object_t *m_obj_array;
  std::size_t m_num_obj;
  APP_GPIO& m_err_led;
  ErrorHandler<APP_GPIO> m_error_handler;
  APP_GPIO& m_con_led;
  ResetFactory &m_reset_factory;
  APP_UniqueID &m_unique_id;
  std::string m_client_name;
  lwm2m_security_instance_data_t m_sec_inst_data;
  lwm2m_server_instance_data_t m_server_inst_data;
  lwm2m_device_instance_data_t m_device_inst_data;
  lwm2m_firmware_update_instance_data_t m_fwup_data;
  APP_Timer m_firmware_running_timer;
  APP_Watchdog &m_watchdog;
  LWM2MLog m_lwm2m_log;
};


#endif /* _APPLICATION_H_ */