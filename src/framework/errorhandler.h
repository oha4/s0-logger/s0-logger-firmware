/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _ERRORHANDLER_H_
#define _ERRORHANDLER_H_

#include <bitset>

enum Errors: std::size_t{
  PARAMETER = 0,
  WIFI,
  LWM2M,
  ERR_LAST
};

template<typename T>
class ErrorHandler{
public:

  /**
   * @brief Construct a new Error Handler object
   * 
   * @param Reference to Error LED
   */
  explicit ErrorHandler(T& err_led) : m_errs{}, m_err_led(err_led){}
  ~ErrorHandler(){};

  /**
   * @brief Set the specified error
   * 
   * @return int 
   */
  int set(Errors err){
    m_errs[err] = true;
    return m_err_led.set();
  }

  /**
   * @brief Clear the specified error
   * 
   * Only if all errors have been cleared the Error LED is cleared. Otherwise the Errors LED stays on.
   * @return int 
   */
  int clear(Errors err){
    m_errs[err] = false;
    if(m_errs.none()){
      return m_err_led.clear();
    }

    return 0;
  }

private:
  std::bitset<Errors::ERR_LAST> m_errs;
  T& m_err_led;
};

#endif /* _ERRORHANDLER_H_ */