/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "application.h"
#include "logger.h"

#include <zephyr/kernel.h>
#include <zephyr/net/net_ip.h>
#include <zephyr/dfu/mcuboot.h>
#include <atomic>

LOG_MODULE_REGISTER(app);

#define SLEEP_TIME_MS   100

std::atomic<bool>  g_firmware_running_30sec = false;
/**
 * @brief Marks currently running image as permanent in case it worked for 30 seconds
 * 
 */
static void firmware_running_successfully(){
  LOG_INF("Firmware successfully running for 30 seconds");
  g_firmware_running_30sec = true;
}

Application::Application(lwm2m_object_t * obj_array, std::size_t num_obj, const LWM2MSettings& lwm2m_settings, FirmwareUpdate fwup, APP_GPIO& err_led, APP_GPIO& con_led, ResetFactory& reset_factory, APP_UniqueID& unique_id, APP_Watchdog& watchdog) :
  m_obj_array(obj_array),
  m_num_obj(num_obj),
  m_err_led(err_led),
  m_error_handler(err_led),
  m_con_led(con_led),
  m_reset_factory(reset_factory),
  m_unique_id(unique_id),
  m_client_name(lwm2m_settings.device_type.data()),
  m_sec_inst_data{
    .uri=lwm2m_settings.server_address.data(),
    .isBootstrap = false,
    .securityMode = SECURITY_MODE_NO_SECURITY,
    .keyOrIdentity = NULL,
    .serverPublicKey = NULL,
    .secretKey = NULL,
    .shortID=123,
    .clientHoldOffTime=10,
    .BootstrapServerAccountTimeout=0
  },
  m_server_inst_data{
    .shortServerId = 123,
    .lifetime = 300,
    .defaultMinObservationPeriod = 10,
    .defaultMaxObservationPeriod = 60,
    .storing = false,
    .binding = BINDING_U
  },
  m_device_inst_data{
    .manufacturer = lwm2m_settings.manufacturer.data(),
    .model = lwm2m_settings.device_type.data(),
    .serial = unique_id.get_string().data(),
    .firmware = VERSION_STR,
    .binding = BINDING_U,
    .deviceType = lwm2m_settings.device_type.data(),
    .hardwareVersion = lwm2m_settings.hardware_version.data(),
    .softwareVersion = VERSION_STR,
  },
  m_fwup_data{
    .write_firmware = fwup.write_firmware,
    .execute_update = fwup.execute_update,
    .pkg_name = "S0-Logger",
    .pkg_version = VERSION_STR
  },
  m_firmware_running_timer(Callback0<void>::from_function<&firmware_running_successfully>()),
  m_watchdog(watchdog),
  m_lwm2m_log(LWM2M_EVENT_LOG_OBJECT_ID, 0,LWM2M_EVENT_LOG_LOG_DATA)
{
  m_client_name += std::string{"-"} + unique_id.get_string().data();
}

int Application::init(){
  LOG_INF("Application Init()");

  /* initialize hardware */
  m_con_led.init();
  m_err_led.init();

  int ret = 0;

  /* create mandatory security object */
  ret = lwm2m_init_object(&m_obj_array[0], LWM2M_SECURITY_OBJECT_ID, lwm2m_security_read, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create security object\r\n");
    m_error_handler.set(Errors::LWM2M);
    return -1;
  }

  lwm2m_instance_t * sec_inst = lwm2m_create_instance(0, &m_sec_inst_data, NULL, false);
  lwm2m_object_add_instance(&m_obj_array[0], sec_inst);

  /* create static server object */
  ret = lwm2m_init_object(&m_obj_array[1], LWM2M_SERVER_OBJECT_ID, lwm2m_server_read, lwm2m_server_write, lwm2m_server_execute, lwm2m_server_create, lwm2m_server_delete, lwm2m_server_discover,NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create server object\r\n");
    m_error_handler.set(Errors::LWM2M);
    return -1;
  }

  lwm2m_instance_t * server_inst = lwm2m_create_instance(0, &m_server_inst_data, NULL, false);
  lwm2m_object_add_instance(&m_obj_array[1], server_inst);

  /* create mandatory device object */
  ret = lwm2m_init_object(&m_obj_array[2], LWM2M_DEVICE_OBJECT_ID, lwm2m_device_read, NULL, lwm2m_device_execute, NULL, NULL, lwm2m_device_discover, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create Device object\r\n");
    m_error_handler.set(Errors::LWM2M);
    return -1;
  }
  
  lwm2m_instance_t * device_inst = lwm2m_create_instance(0, &m_device_inst_data, NULL, false);
  lwm2m_object_add_instance(&m_obj_array[2], device_inst);

    /* --------------------------- */
  /* create Firmware Update instance */
  /* --------------------------- */
  ret = lwm2m_init_object(&m_obj_array[3], LWM2M_FIRMWARE_UPDATE_OBJECT_ID, lwm2m_firmware_update_read, NULL, lwm2m_firmware_update_execute, NULL, NULL, NULL, lwm2m_firmware_update_write, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create Firmware Update object\r\n");
    m_error_handler.set(Errors::LWM2M);
    return -1;
  }
  
  lwm2m_instance_t * fwup = lwm2m_create_instance(0, &m_fwup_data, NULL, false);
  lwm2m_object_add_instance(&m_obj_array[3], fwup);

  /* --------------------------- */
  /* create Log instance */
  /* --------------------------- */
  ret = lwm2m_init_object(&m_obj_array[4], LWM2M_EVENT_LOG_OBJECT_ID, LWM2MLog::lwm2m_log_read, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  if(ret < 0){
    LOG_ERROR("Failed to create Log object");
    m_error_handler.set(Errors::LWM2M);
    return -1;
  }

  lwm2m_instance_t* log = lwm2m_create_instance(0, nullptr, nullptr, false);
  lwm2m_object_add_instance(&m_obj_array[4], log);

  m_lwm2m_log.registerObserver(&m_observer);

  m_firmware_running_timer.init();
  m_watchdog.init();

  this->tech_init(&m_obj_array[5], m_num_obj -5);

  return 0;
}

int Application::start(){
  LOG_INF("Application Start()");

  LOG_INF("Client name: %s", m_client_name.data());
  m_lwm2mH = lwm2m_create_context(m_obj_array, m_num_obj, m_client_name.data(), "56830", AF_INET, 2000);
  if(m_lwm2mH == NULL){
    LOG_ERROR("Could not create lwm2m context");
    m_error_handler.set(Errors::LWM2M);
    return -1;
  }

  m_observer.setContext(m_lwm2mH);
  m_con_led.set();
  tech_start();

  m_firmware_running_timer.start(30);

  while (1){
    static lwm2m_firmware_update_state_t old_state;
    lwm2m_process(m_lwm2mH);
    lwm2m_object_t* obj =  (lwm2m_object_t*)LWM2M_LIST_FIND(m_lwm2mH->objectList, 5);
    if(obj != NULL){
      lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0);
      if(inst != NULL){
        lwm2m_firmware_update_instance_data_t* data = (lwm2m_firmware_update_instance_data_t*)inst->data;
        if(data->state != old_state){
          lwm2m_uri_t uri;
          lwm2m_stringToUri("/5/0/3", 6, &uri);
          lwm2m_resource_value_changed(m_lwm2mH, &uri);
          LOG_INF("FWUP State: %d", data->state);
          old_state = data->state;
        }
      }
    }


    // k_msleep(10);
    if(m_lwm2mH->state == lwm2m_client_state_t::STATE_READY){
      m_con_led.set();
    }else{
      m_con_led.clear();
    }

    if(g_firmware_running_30sec == true) boot_write_img_confirmed();

    m_watchdog.trigger();
  }
}


int Application::stop(){
  tech_stop();
  return 0;
}