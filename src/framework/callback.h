/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef CALLBACK_H
#define CALLBACK_H

template <typename R>
class CallbackInvoker0;

template <typename R>
class Callback0
{
public:
  typedef R return_type;

  Callback0()
      : object_ptr(0), stub_ptr(0){
  }

  template <return_type (*TMethod)()>
  static Callback0 from_function(){
    return from_stub(0, &function_stub<TMethod>);
  }

  template <class T, return_type (T::*TMethod)()>
  static Callback0 from_method(T *object_ptr){
    return from_stub(object_ptr, &method_stub<T, TMethod>);
  }

  template <class T, return_type (T::*TMethod)() const>
  static Callback0 from_const_method(T const *object_ptr){
    return from_stub(const_cast<T *>(object_ptr), &const_method_stub<T, TMethod>);
  }

  return_type operator()() const{
    return (*stub_ptr)(object_ptr);
  }

  operator bool() const{
    return stub_ptr != 0;
  }

  bool operator!() const{
    return !(operator bool());
  }

private:
  typedef return_type (*stub_type)(void *object_ptr);

  void *object_ptr;
  stub_type stub_ptr;

  static Callback0 from_stub(void *object_ptr, stub_type stub_ptr){
    Callback0 d;
    d.object_ptr = object_ptr;
    d.stub_ptr = stub_ptr;
    return d;
  }

  template <return_type (*TMethod)()>
  static return_type function_stub(void *){
    return (TMethod)();
  }

  template <class T, return_type (T::*TMethod)()>
  static return_type method_stub(void *object_ptr){
    T *p = static_cast<T *>(object_ptr);
    return (p->*TMethod)();
  }

  template <class T, return_type (T::*TMethod)() const>
  static return_type const_method_stub(void *object_ptr){
    T const *p = static_cast<T *>(object_ptr);
    return (p->*TMethod)();
  }
};


template <typename R>
class CallbackInvoker0{

public:
  CallbackInvoker0(){}

  template <class TDelegate>
  R operator()(TDelegate d) const{
    return d();
  }
};

template <typename R, typename A1>
class CallbackInvoker1;

template <typename R, typename A1>
class Callback1
{
public:
  typedef R return_type;

  Callback1()
      : object_ptr(0), stub_ptr(0){
  }

  template <return_type (*TMethod)(A1)>
  static Callback1 from_function(){
    return from_stub(0, &function_stub<TMethod>);
  }

  template <class T, return_type (T::*TMethod)(A1)>
  static Callback1 from_method(T *object_ptr){
    return from_stub(object_ptr, &method_stub<T, TMethod>);
  }

  template <class T, return_type (T::*TMethod)(A1) const>
  static Callback1 from_const_method(T const *object_ptr){
    return from_stub(const_cast<T *>(object_ptr), &const_method_stub<T, TMethod>);
  }

  return_type operator()(A1 a1) const{
    return (*stub_ptr)(object_ptr, a1);
  }

  operator bool() const{
    return stub_ptr != 0;
  }

  bool operator!() const{
    return !(operator bool());
  }

private:
  typedef return_type (*stub_type)(void *object_ptr, A1);

  void *object_ptr;
  stub_type stub_ptr;

  static Callback1 from_stub(void *object_ptr, stub_type stub_ptr){
    Callback1 d;
    d.object_ptr = object_ptr;
    d.stub_ptr = stub_ptr;
    return d;
  }

  template <return_type (*TMethod)(A1)>
  static return_type function_stub(void *, A1 a1){
    return (TMethod)(a1);
  }

  template <class T, return_type (T::*TMethod)(A1)>
  static return_type method_stub(void *object_ptr, A1 a1){
    T *p = static_cast<T *>(object_ptr);
    return (p->*TMethod)(a1);
  }

  template <class T, return_type (T::*TMethod)(A1) const>
  static return_type const_method_stub(void *object_ptr, A1 a1){
    T const *p = static_cast<T *>(object_ptr);
    return (p->*TMethod)(a1);
  }
};

template <typename R, typename A1>
class CallbackInvoker1{
  A1 a1;

public:
  explicit CallbackInvoker1(A1 a1)
      : a1(a1){
  }

  template <class TDelegate>
  R operator()(TDelegate d) const{
    return d(a1);
  }
};

template <typename R, typename A1, typename A2>
class CallbackInvoker2;

template <typename R, typename A1, typename A2>
class Callback2
{
public:
  typedef R return_type;

  Callback2()
      : object_ptr(0), stub_ptr(0){
  }

  template <return_type (*TMethod)(A1, A2)>
  static Callback2 from_function(){
    return from_stub(0, &function_stub<TMethod>);
  }

  template <class T, return_type (T::*TMethod)(A1, A2)>
  static Callback2 from_method(T *object_ptr){
    return from_stub(object_ptr, &method_stub<T, TMethod>);
  }

  template <class T, return_type (T::*TMethod)(A1, A2) const>
  static Callback2 from_const_method(T const *object_ptr){
    return from_stub(const_cast<T *>(object_ptr), &const_method_stub<T, TMethod>);
  }

  return_type operator()(A1 a1, A2 a2) const{
    return (*stub_ptr)(object_ptr, a1, a2);
  }

  operator bool() const{
    return stub_ptr != 0;
  }

  bool operator!() const{
    return !(operator bool());
  }

private:
  typedef return_type (*stub_type)(void *object_ptr, A1, A2);

  void *object_ptr;
  stub_type stub_ptr;

  static Callback2 from_stub(void *object_ptr, stub_type stub_ptr){
    Callback2 d;
    d.object_ptr = object_ptr;
    d.stub_ptr = stub_ptr;
    return d;
  }

  template <return_type (*TMethod)(A1, A2)>
  static return_type function_stub(void *, A1 a1, A2 a2){
    return (TMethod)(a1, a2);
  }

  template <class T, return_type (T::*TMethod)(A1, A2)>
  static return_type method_stub(void *object_ptr, A1 a1, A2 a2){
    T *p = static_cast<T *>(object_ptr);
    return (p->*TMethod)(a1, a2);
  }

  template <class T, return_type (T::*TMethod)(A1, A2) const>
  static return_type const_method_stub(void *object_ptr, A1 a1, A2 a2){
    T const *p = static_cast<T *>(object_ptr);
    return (p->*TMethod)(a1, a2);
  }
};

template <typename R, typename A1, typename A2>
class CallbackInvoker2{
  A1 a1;
  A2 a2;

public:
  CallbackInvoker2(A1 a1, A2 a2)
      : a1(a1), a2(a2){
  }

  template <class TDelegate>
  R operator()(TDelegate d) const{
    return d(a1, a2);
  }
};

#endif /* CALLBACK_H */