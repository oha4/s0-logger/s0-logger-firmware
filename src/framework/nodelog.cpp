/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "nodelog.h"
#include "fifo.h"
#if defined(CONFIG_PIGWEED_LOG_TOKENIZED)
    #include <pw_log/log.h>
#else
#include <zephyr/logging/log_backend.h>
#include <zephyr/logging/log_output.h>
#include <zephyr/logging/log_backend_std.h>
#endif
#include <zephyr/kernel.h>

#include <memory>
#include <algorithm>

static LWM2MLog* g_log_inst = nullptr;

LWM2MLog::LWM2MLog(unsigned int object_id, unsigned int instance_id, unsigned int resource_id) : IObservableBase(object_id, instance_id, resource_id)
{
    g_log_inst = this;
}

static const std::size_t BUFFER_SIZE = 4 * 1024;
static uint8_t trace_buffer[BUFFER_SIZE];
FIFO trace_store(std::move(std::unique_ptr<uint8_t[]>(trace_buffer)), BUFFER_SIZE);

uint8_t LWM2MLog::lwm2m_log_read(uint16_t instance_id, int *num_data, lwm2m_data_t **dataArrayP, lwm2m_object_t *obj)
{
    lwm2m_instance_t *inst = (lwm2m_instance_t *)lwm2m_list_find(obj->instanceList, instance_id);
    if (NULL == inst)
    {
        // LOG_ERR("Instance %d not found", instance_id);
        return COAP_404_NOT_FOUND;
    }

    uint8_t buffer[128] = {};
    size_t len = trace_store.pop(buffer, 128);
    // printk("Read Log\n");
    lwm2m_data_encode_opaque(buffer, len, *dataArrayP);
    // printk("Send Logdata with length: %d and type: %d\n", (*dataArrayP)->value.asBuffer.length, (*dataArrayP)->type);
    *num_data = 1;

    return COAP_205_CONTENT;
}

#if defined(CONFIG_PIGWEED_LOG_TOKENIZED)
extern "C" void pw_log_tokenized_HandleLog(uint32_t payload, const uint8_t message[], size_t size){
    // The metadata object provides the log level, module token, and flags.
    // These values can be recorded and used for runtime filtering.
    pw::log_tokenized::Metadata metadata(payload);

    switch(metadata.level())
    {
        case PW_LOG_LEVEL_CRITICAL:
        case PW_LOG_LEVEL_ERROR:
        case PW_LOG_LEVEL_WARN:
        case PW_LOG_LEVEL_FATAL:
        case PW_LOG_LEVEL_INFO:
        {
            printk("[%d] %d:%d \n", metadata.level() , metadata.module(), metadata.line_number());
            uint8_t buf[64] = {};
            buf[0] = size & 0xFF;
            std::copy(message, message+size, buf+1);
            trace_store.push(buf, size+1);

            if (g_log_inst)
            {
                g_log_inst->notifyObservers();
            }
            break;
        }

        default:
        {
            /* do nothing */
            break;
        }
    }
    
    return;
}
#else
static uint8_t buf[256];
static uint32_t log_format_current = 0;
static int char_out(uint8_t *data, size_t length, void *ctx)
{
    // printk("Received log with length %d bytes\n", length);
    trace_store.push(data, length);

    if (g_log_inst)
    {
        g_log_inst->notifyObservers();
    }

    return length;
}

LOG_OUTPUT_DEFINE(log_output_lwm2m, char_out, buf, sizeof(buf));

static void process(const struct log_backend *backend,
                                   union log_msg_generic *msg)
{
    uint32_t flags = log_backend_std_get_flags();

    log_format_func_t log_output_func = log_format_func_t_get(log_format_current);

    log_output_func(&log_output_lwm2m, &msg->log, flags);
}

static const struct log_backend_api lwm2m_backend_api = {
    .process = process,
};

LOG_BACKEND_DEFINE(log_backend_lwm2m, lwm2m_backend_api, true);
#endif