/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef NODELOG_H_
#define NODELOG_H_

#include "observer.h"

#include <cstdint>
#include <ulwm2m.h>


/**
 * @brief Ressource ID's for LWM2M Device Object
 *
 */
#define LWM2M_EVENT_LOG_OBJECT_ID 20
#define LWM2M_EVENT_LOG_LOG_DATA 4014


class LWM2MLog : public IObservableBase{
public:
    LWM2MLog(unsigned int object_id, unsigned int instance_id, unsigned int resource_id);
    static uint8_t lwm2m_log_read(uint16_t instance_id, int *num_data, lwm2m_data_t **dataArrayP, lwm2m_object_t *obj);
    
protected:
};
#endif /* NODELOG_H_ */