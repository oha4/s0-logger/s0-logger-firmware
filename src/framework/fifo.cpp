/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/

/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */
#include "fifo.h"

#include <cstdint>
#include <cstdlib>

FIFO::FIFO(std::unique_ptr<uint8_t[]> buffer, size_t size)
    : buffer_(std::move(buffer)), size_(size), head_(0), tail_(0), empty_(true)
{
    if (!buffer_ || size_ == 0)
    {
        valid_ = false;
    }
    else
    {
        valid_ = true;
    }
}

FIFO::FIFO(FIFO &&other) noexcept
    : buffer_(std::move(other.buffer_)),
      size_(other.size_),
      head_(other.head_),
      tail_(other.tail_),
      empty_(other.empty_),
      valid_(other.valid_)
{
    other.size_ = 0;
    other.head_ = 0;
    other.tail_ = 0;
    other.empty_ = true;
    other.valid_ = false;
}

FIFO &FIFO::operator=(FIFO &&other) noexcept
{
    if (this != &other)
    {
        buffer_ = std::move(other.buffer_);
        size_ = other.size_;
        head_ = other.head_;
        tail_ = other.tail_;
        empty_ = other.empty_;
        valid_ = other.valid_;

        other.size_ = 0;
        other.head_ = 0;
        other.tail_ = 0;
        other.empty_ = true;
        other.valid_ = false;
    }
    return *this;
}

size_t FIFO::push(const uint8_t *data, size_t length)
{
    if (!valid_ || length == 0 || !data)
    {
        return 0;
    }

    // Check if there is enough space to add the size byte and the data
    size_t totalSize = length + 1; // +1 for the size byte
    if (freeSpace() < totalSize)
    {
        discardElements(totalSize - freeSpace());
    }

    // Write the size byte
    buffer_[head_] = static_cast<uint8_t>(length);
    head_ = (head_ + 1) % size_;

    // Write the data bytes with wrap-around logic
    for (size_t i = 0; i < length; ++i)
    {
        buffer_[head_] = data[i];
        head_ = (head_ + 1) % size_;
    }

    empty_ = false;
    return length;
}

size_t FIFO::pop(uint8_t *dest, size_t maxLength)
{
    if (!valid_ || maxLength == 0 || !dest)
    {
        return 0;
    }

    if (isEmpty())
    {
        return 0;
    }

    size_t bytesRead = 0;
    size_t currentTail = tail_;
    size_t remaining_bytes = maxLength;
    while (currentTail != head_ && bytesRead < maxLength)
    {
        // Read the size byte
        size_t elementSize = buffer_[currentTail];
        currentTail = (currentTail + 1) % size_;

        if(elementSize > remaining_bytes){
            break;
        }

        // Read the data bytes
        for (size_t i = 0; i < elementSize && bytesRead < maxLength; ++i)
        {
            dest[bytesRead++] = buffer_[currentTail];
            currentTail = (currentTail + 1) % size_;
        }
        remaining_bytes -= bytesRead;
        tail_ = currentTail;
    }

    return bytesRead;
}

bool FIFO::isEmpty() const
{
    return empty_;
}

size_t FIFO::usedSpace() const
{
    if (head_ >= tail_)
    {
        return head_ - tail_;
    }
    else
    {
        return size_ - (tail_ - head_);
    }
}

size_t FIFO::freeSpace() const
{
    return size_ - usedSpace();
}

void FIFO::discardElements(size_t requiredSpace)
{
    ssize_t required = requiredSpace;
    while (required > 0 && tail_ != head_)
    {
        size_t elementSize = buffer_[tail_] + 1; // +1 for the size byte
        tail_ = (tail_ + elementSize) % size_;
        required -= elementSize;
    }
}

size_t FIFO::size() const{
    return size_;
}