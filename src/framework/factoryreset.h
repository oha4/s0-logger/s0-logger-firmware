/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef FACTORYRESET_H
#define FACTORYRESET_H

#include "hw_interfaces.h"
#include "callback.h"
#include <zephyr/kernel.h>
#include "logger.h"

#include <vector>

template <typename GPIO>
class FactoryReset{
public:
  FactoryReset(GPIO btn) : m_button(btn){
  }

  void addCallback(uint32_t wait_time_milliseconds, Callback0<void> &&cb)
  {
    m_button.init(Callback2<void, uint32_t, bool>::from_method<FactoryReset, &FactoryReset::button_callback>(this), GPIO::Edge::BOTH);

    m_callbacks.push_back(CallbackEntry{.duration = wait_time_milliseconds, .cb = cb});
    std::sort(m_callbacks.begin(), m_callbacks.end(), [](CallbackEntry a, CallbackEntry b) {
      return a.duration > b.duration;
    });
  }

protected:
  void button_callback(uint32_t pin, bool edge){
    if(edge){
      m_button_pressed_time = k_uptime_get_32();
    }else{
      uint32_t time_passed = k_uptime_get_32() - m_button_pressed_time;
      printk("Time passed %d ms\n", time_passed);
      for(unsigned int i=0; i< m_callbacks.size();i++) {
        if(time_passed > m_callbacks[i].duration) {
          m_callbacks[i].cb();
          break;
        }
      }
    }
  }

private:
  struct CallbackEntry {
    uint32_t duration;
    Callback0<void> cb;
  };

  GPIO m_button;
  uint32_t m_button_pressed_time;
  std::vector<CallbackEntry> m_callbacks;
};
#endif /* FACTORYRESET_H */