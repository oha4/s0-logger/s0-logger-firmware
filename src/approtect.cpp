/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "approtect.h"

#include <cstdint>

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/drivers/flash.h>

LOG_MODULE_REGISTER(approtect);

static void nrf_nvmc_write_word(uint32_t address, uint32_t value)
{
	// Enable flash write access and wait until the NVMC is ready
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}

    // Write to the register and wait until the NVMC is ready
    *(uint32_t*)address = value;
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}

	// Disable flash write access and wait until the NVMC is ready
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}
}

void enable_approtect(void){
  LOG_INF("Chip Variant: 0x%08x", NRF_FICR->INFO.VARIANT);
  if (NRF_UICR->APPROTECT == 0xFFFFFFFF)
    {
		LOG_WRN("Access Port Protection is not enabled --> Enable Access Port Protection now");
		/* Enable Access Port Protection
		 * Access through debugger to CPU registers, mapped-memory and RAM will be disabled
		 * To disable protection ERASEALL command must be applied. */
		nrf_nvmc_write_word((uint32_t)&NRF_UICR->APPROTECT, 0xFFFFFF00);
		LOG_INF("Access Port Protection is now enabled --> Reboot to apply the config...");

		// Sleep is only necessary to show the logs before reboot for debug purposes
		k_sleep(K_MSEC(2000));

		sys_reboot(SYS_REBOOT_COLD);
    }
	else
	{
		LOG_INF("Access Port Protection is already enabled");
	}
}