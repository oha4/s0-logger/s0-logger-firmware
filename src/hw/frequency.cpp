/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "frequency.h"
#include <zephyr/logging/log.h>
#include <zephyr/kernel.h>

LOG_MODULE_REGISTER(Frequency);

// Define factory method
ZephyrFrequency::ZephyrFrequency(const struct gpio_dt_spec &dt, unsigned int object_id, unsigned int instance_id, unsigned int ressource_id) : IObservableBase(object_id, instance_id, ressource_id), m_val(0), m_tmp_val(0), m_gpio(dt)
{
  this->cb_store.obj = this;
}

void ZephyrFrequency::gpio_handler(const struct device *port, struct gpio_callback *cb, gpio_port_pins_t pins){
  ZephyrFrequency::callbackStore* store = CONTAINER_OF(cb, ZephyrFrequency::callbackStore, gpio_cb);
  ZephyrFrequency * impl = store->obj;
  impl->m_tmp_val++;
}

// Define public method
float ZephyrFrequency::get(void)
{
    return m_val;
}

int ZephyrFrequency::init(uint32_t measurement_time_ms){
  int ret = gpio_pin_configure_dt(&m_gpio, m_gpio.dt_flags);
  if(ret != 0){
    LOG_ERR("Could not configure GPIO %d. Ret: %d", m_gpio.pin, ret);
    return -1;
  }

  ret = gpio_pin_is_input_dt(&m_gpio);
  if(ret != 0){
    LOG_ERR("Pin %d is not input. ret: %d", m_gpio.pin, ret);
    return -1;
  }

  ret = gpio_pin_interrupt_configure_dt(&m_gpio, GPIO_INT_ENABLE | GPIO_INT_HIGH_1 | GPIO_INT_EDGE);
  if (ret) {
      LOG_ERR("Could not configure interrupt. Ret: %d", ret);
      return -1;
  }

  gpio_init_callback(&cb_store.gpio_cb, gpio_handler, BIT(m_gpio.pin));

  ret = gpio_add_callback(m_gpio.port, &cb_store.gpio_cb);
  if(ret != 0){
    LOG_ERR("Could not add callback. Ret: %d", ret);
    return -1;
  }

  k_timer_init(&m_timer, timer_expired_callback, NULL);
  k_timer_user_data_set(&m_timer, this);
  k_timer_start(&m_timer, K_MSEC(measurement_time_ms), K_MSEC(measurement_time_ms));

  return 0;
}

void ZephyrFrequency::timer_expired_callback(struct k_timer *timer_id){
  ZephyrFrequency* freq = (ZephyrFrequency*)k_timer_user_data_get(timer_id);
  // LOG_INF("Timer for obj 0x%p expired. Val: %f", freq, freq->val);
  freq->m_val = freq->m_tmp_val;
  freq->m_tmp_val = 0;

  freq->notifyObservers();
}
