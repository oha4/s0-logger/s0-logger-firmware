/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "gpio.h"
#include <zephyr/sys/util.h>

ZephyrGPIO::ZephyrGPIO(struct gpio_dt_spec gpio): m_gpio(gpio), m_state(State::OFF){

}

int ZephyrGPIO::init(){
  if(!gpio_is_ready_dt(&m_gpio)){
    return -1;  
  }

  if(gpio_pin_configure_dt(&m_gpio, GPIO_OUTPUT_ACTIVE) < 0){
    return -1;
  }

  if(clear() != 0){
    return -1;
  }

  return 0;
}

int ZephyrGPIO::set(){
  if(gpio_pin_set_dt(&m_gpio, true) != 0){
    return -1;
  }

  m_state = GPIO::ON;
  return 0;
}

int ZephyrGPIO::clear(){
    if(gpio_pin_set_dt(&m_gpio, false) != 0){
    return -1;
  }

  m_state = GPIO::OFF;
  return 0;
}

GPIO<ZephyrGPIO>::State ZephyrGPIO::getState(){
  return m_state;
}

ZephyrGPIO_I::ZephyrGPIO_I(struct gpio_dt_spec dts): m_gpio(dts){

}

int ZephyrGPIO_I::init(){
  if(!gpio_is_ready_dt(&m_gpio)){
    return -1;  
  }

  if(gpio_pin_configure_dt(&m_gpio, GPIO_INPUT) < 0){
    return -1;
  }

  return 0;
}

bool ZephyrGPIO_I::get(){
  int ret = gpio_pin_get_dt(&m_gpio);
  if(ret!= 0){
    return true;
  }else{
    return false;
  }
}

ZephyrGPIOInterrupt::ZephyrGPIOInterrupt(struct gpio_dt_spec dts) : m_gpio(dts){

}

void ZephyrGPIOInterrupt::interrupt_callback(const struct device *port, struct gpio_callback *cb, gpio_port_pins_t pins){
  ZephyrGPIOInterrupt::callbackStore* store = CONTAINER_OF(cb, ZephyrGPIOInterrupt::callbackStore, gpio_cb);
  ZephyrGPIOInterrupt * impl = store->obj;
  gpio_port_value_t val;
  gpio_port_get(port, &val);
  impl->m_cb_fnc(pins, val & pins);
}

int ZephyrGPIOInterrupt::init(Callback2<void, uint32_t, bool> cb, GPIOInterrupt::Edge edge){
  if(!gpio_is_ready_dt(&m_gpio)){
    return -1;
  }

  if(gpio_pin_configure_dt(&m_gpio, GPIO_INPUT) != 0){
    return -1;
  }

  int ret = 0;
  switch(edge){
    case Edge::POSITIVE:
    {
      ret = gpio_pin_interrupt_configure_dt(&m_gpio, GPIO_INT_EDGE_TO_ACTIVE);
      break;
    }

    case Edge::NEGATIVE:
    {
      ret = gpio_pin_interrupt_configure_dt(&m_gpio, GPIO_INT_EDGE_TO_INACTIVE);
      break;
    }

    case Edge::BOTH:
    {
      ret = gpio_pin_interrupt_configure_dt(&m_gpio, GPIO_INT_EDGE_BOTH);
      break;
    }
  }

  if(ret != 0){
    return -1;
  }

  gpio_init_callback(&m_cb_store.gpio_cb, interrupt_callback, BIT(m_gpio.pin));
	if(gpio_add_callback(m_gpio.port, &m_cb_store.gpio_cb) != 0){
    return -1;
  }
  m_cb_store.obj = this;
  m_cb_fnc = cb;

  return 0;
}