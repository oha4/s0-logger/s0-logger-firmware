/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef LED_H_
#define LED_H_

#include <hw_interfaces.h>
#include <zephyr/drivers/gpio.h>
#include <functional>

/**
 * @brief Zephyr GPIO
 * 
 */
class ZephyrGPIO : public GPIO<ZephyrGPIO>{
public:

  ZephyrGPIO(struct gpio_dt_spec gpio);
  /**
   * @brief Initialize gpio
   * 
   * @return Returns 0 if initialization succeeds, -1 on failure.
   */
  int init();

  GPIO<ZephyrGPIO>::State getState();
  int set();
  int clear();

private:
  struct gpio_dt_spec m_gpio;
  GPIO<ZephyrGPIO>::State m_state;
};

/**
 * @brief Zephyr GPIO
 * 
 */
class ZephyrGPIO_I final : public GPIO_I<ZephyrGPIO_I>{
public:
  ZephyrGPIO_I(struct gpio_dt_spec dts);

  /**
   * @brief Initialize gpio
   * 
   * @return Returns 0 if initialization succeeds, -1 on failure.
   */
  int init();

  bool get();

private:
  struct gpio_dt_spec m_gpio;
};

class ZephyrGPIOInterrupt : public GPIOInterrupt<ZephyrGPIOInterrupt>{
public:
  ZephyrGPIOInterrupt(struct gpio_dt_spec dts);

  int init(Callback2<void, uint32_t, bool> cb, GPIOInterrupt::Edge edge);

protected:
  static void interrupt_callback(const struct device *port, struct gpio_callback *cb, gpio_port_pins_t pins);

private:
  struct gpio_dt_spec m_gpio;
  Callback2<void, uint32_t, bool> m_cb_fnc;
  struct callbackStore{
    struct gpio_callback gpio_cb;
    ZephyrGPIOInterrupt* obj;
  }m_cb_store;
};

///
/// Implementations
///
// template<class gpio_dt>
// int ZephyrGPIO<class gpio_dt>::init(){
//   if(!gpio_is_ready_dt(&gpio_dt)){
//     return -1;  
//   }

//   if(gpio_pin_configure_dt(&gpio_dt, GPIO_OUTPUT_ACTIVE) < 0){
//     return -1;
//   }

//   if(clear() != 0){
//     return -1;
//   }

//   return 0;
// }

// template<class gpio_dt>
// int ZephyrGPIO<class gpio_dt>::set(){
//   if(gpio_pin_set_dt(&gpio_dt, true) != 0){
//     return -1;
//   }

//   m_state = GPIO::ON;
//   return 0;
// }

// template<class gpio_dt>
// int ZephyrGPIO<class gpio_dt>::clear(){
//     if(gpio_pin_set_dt(&gpio_dt, false) != 0){
//     return -1;
//   }

//   m_state = GPIO::OFF;
//   return 0;
// }

// template<class gpio_dt>
// GPIO<ZephyrGPIO<class gpio_dt> >::State ZephyrGPIO<class gpio_dt>::getState(){
//   return m_state;
// }
#endif /* LED_H_ */