/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef UNIQUE_ID_H
#define UNIQUE_ID_H

#include <hw_interfaces.h>

#include <string_view>
#include <array>

class ZephyrUniqueID final: public UniqueID<ZephyrUniqueID> {
public:
  ZephyrUniqueID();
  std::string_view get_string() const;
  std::array<uint8_t, 8> get() const;

private:
  std::array<uint8_t, 8> m_id;
  std::array<char, 8*2+1> m_id_str;
};

#endif /* UNIQUE_ID_H */