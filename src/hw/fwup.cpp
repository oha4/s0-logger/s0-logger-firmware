/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "fwup.h"

#include <zephyr/logging/log.h>
#include <zephyr/dfu/flash_img.h>
#include <zephyr/storage/flash_map.h>
#include <zephyr/dfu/mcuboot.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/kernel.h>

LOG_MODULE_REGISTER(fwup);



uint32_t write_firmware(uint8_t * buffer, int length, uint32_t block_num, uint8_t block_more){
  int ret;
  uint8_t id = DT_FIXED_PARTITION_ID(DT_NODE_BY_FIXED_PARTITION_LABEL(image_1));
  static struct flash_img_context ctx;
  if(block_num == 0){
    LOG_INF("Firmware update started");
    // erase of image causes firmware transfer to fail. It always retries with block 0. Maybe timeout?
    // if(boot_erase_img_bank(id) != 0){
    //   LOG_ERR("Could not erase image in partition 1");
    //   return 1;
    // }
    if(flash_img_init_id(&ctx, id) != 0){
      LOG_ERR("Could not open Flash partition 1");
      return 1;
    }
  }

  ret = flash_img_buffered_write(&ctx, buffer, length, !block_more);

  return ret;
}

uint32_t execute_update(void){
  LOG_INF("Execute Update");
   
  int ret = boot_request_upgrade(BOOT_UPGRADE_TEST);
  if(ret != 0){
    LOG_ERR("Could not request upgrade");
  }else{
    sys_reboot(SYS_REBOOT_COLD);
  }

  return ret;
}