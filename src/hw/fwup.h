/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _FWUP_H_
#define _FWUP_H_

#include <cstdint>

uint32_t write_firmware(uint8_t * buffer, int length, uint32_t block_num, uint8_t block_more);

uint32_t execute_update(void); 

#endif /* _FWUP_H_ */