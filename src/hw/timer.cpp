/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "timer.h"

void ZephyrTimer::callback(struct k_timer *timer_id){
  ZephyrTimer::callbackStore* store = CONTAINER_OF(timer_id, ZephyrTimer::callbackStore, timer);
  ZephyrTimer * impl = store->obj;
  impl->m_cb();
}

ZephyrTimer::ZephyrTimer(Callback0<void> cb): m_cb_store{.timer = {}, .obj = this}, m_cb(cb){

}

int ZephyrTimer::init(){
  k_timer_init(&m_cb_store.timer, callback, NULL);

  return 0;
}

void ZephyrTimer::start(uint32_t seconds){
  k_timer_start(&m_cb_store.timer, K_SECONDS(seconds), K_FOREVER);
}

void ZephyrTimer::stop(void){
  k_timer_stop(&m_cb_store.timer);
}