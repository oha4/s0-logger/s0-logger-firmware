/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "uniqueid.h"

#include <zephyr/drivers/hwinfo.h>
#include <cstdlib>

ZephyrUniqueID::ZephyrUniqueID() : m_id_str{'0'}{
  hwinfo_get_device_id(m_id.data(), m_id.max_size());
  auto it = m_id_str.begin();
  for (const auto& val : m_id) {
    *it++ = "0123456789ABCDEF"[val >> 4];
    *it++ = "0123456789ABCDEF"[val & 0x0F];
  }
}

std::string_view ZephyrUniqueID::get_string() const{
  return std::string_view(m_id_str.data());
}

std::array<uint8_t, 8> ZephyrUniqueID::get() const{
  return m_id;
}
