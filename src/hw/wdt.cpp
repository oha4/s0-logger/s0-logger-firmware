/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "wdt.h"
#include <zephyr/logging/log.h>

LOG_MODULE_REGISTER(watchdog);

ZephyrWatchdog::ZephyrWatchdog(const struct device *wdt_dev) : wdt_dev_(wdt_dev) {
  if (!wdt_dev_) {
    return;
  }
}

bool ZephyrWatchdog::init(const uint32_t timeout) {
  wdt_config.flags = WDT_FLAG_RESET_SOC;
  wdt_config.window.min = 0U;
  wdt_config.window.max = timeout;
  wdt_config.callback = NULL;

  if (wdt_install_timeout(wdt_dev_, &wdt_config) < 0) {
      LOG_ERR("Unable to install watchdog timeout");
      return false;
  }

  // Setup the Watchdog
  if (wdt_setup(wdt_dev_, WDT_OPT_PAUSE_HALTED_BY_DBG) < 0) {
      LOG_ERR("Unable to setup the watchdog\n");
      return false;
  }

  return true;
}

void ZephyrWatchdog::trigger() {
  // Zyklisch den Watchdog triggern, um einen Reset zu verhindern
  wdt_feed(wdt_dev_, 0);
}