/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2025 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "reset.h"

#include <zephyr/kernel.h>
#include <zephyr/sys/reboot.h>

void ZephyrReset::reboot()
{
    sys_reboot(SYS_REBOOT_COLD);
}