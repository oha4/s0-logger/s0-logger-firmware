/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef WDT_H_
#define WDT_H_

#include <hw_interfaces.h>
#include <zephyr/kernel.h>
#include <zephyr/drivers/watchdog.h>

/**
 * @brief Concrete implementation of the Watchdog class for Watchdog control.
 */
class ZephyrWatchdog : public Watchdog<ZephyrWatchdog>{
public:
    ZephyrWatchdog(const struct device *wdt_dev);

    /**
     * @brief Implementation of the initialization method for the Watchdog.
     * 
     * @return true if initialization was successful, otherwise false.
     */
    bool init(const uint32_t timeout=10000);

    /**
     * @brief Implementation of the trigger method for the Watchdog.
     */
    void trigger();

private:
    const struct device *wdt_dev_;
    struct wdt_timeout_cfg wdt_config;
};

#endif /* WDT_H_ */