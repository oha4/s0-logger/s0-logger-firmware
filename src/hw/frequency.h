/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _FREQUENCY_H_
#define _FREQUENCY_H_

#include <hw_interfaces.h>
#include <cstdint>
#include <functional>
#include <observer.h>

#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>

class ZephyrFrequency : public Frequency<ZephyrFrequency>, public IObservableBase
{
public:
  ZephyrFrequency(const struct gpio_dt_spec& dt, unsigned int object_id, unsigned int instance_id, unsigned int ressource_id);
  ~ZephyrFrequency() = default;

  int init(uint32_t measurement_time_ms);
  float get(void);

protected:
  static void gpio_handler(const struct device *port, struct gpio_callback *cb, gpio_port_pins_t pins);
  static void timer_expired_callback(struct k_timer *timer_id);

private:
  struct callbackStore{
      struct gpio_callback gpio_cb;
      ZephyrFrequency* obj;
    }cb_store;


    float m_val;
    float m_tmp_val;
    struct gpio_dt_spec m_gpio;
    struct k_timer m_timer;
};

#endif /* _FREQUENCY_H_ */