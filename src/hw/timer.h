/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef TIMER_H_
#define TIMER_H_

#include <hw_interfaces.h>
#include <zephyr/kernel.h>
#include <functional>

class ZephyrTimer : public Timer<ZephyrTimer>{
public:
  explicit ZephyrTimer(Callback0<void> cb);
  int init();
  void start(uint32_t seconds);
  void stop(void);

protected:
  static void callback(struct k_timer *timer_id);

private:
  struct callbackStore{
    struct k_timer timer;
    ZephyrTimer* obj;
  }m_cb_store;
  Callback0<void> m_cb;

};

#endif /* TIMER_H_ */