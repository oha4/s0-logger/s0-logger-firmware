/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef TECH_H_
#define TECH_H_

#include <application.h>
#include <hw_cfg.h>
#include "fwup.h"

class Tech final : public Application{
public:
  Tech(lwm2m_object_t *obj_array, std::size_t m_num_obj, const LWM2MSettings lwm2m_settings, FirmwareUpdate fwup, APP_GPIO &err_led, APP_GPIO &con_led, ResetFactory &reset_factory, APP_UniqueID &unique_id, const uint32_t imp_per_kwh, APP_Watchdog &watchdog, APP_Frequency &s0_input_0, APP_Frequency &s0_input_1);
  ~Tech(){};

protected:
  int tech_init(lwm2m_object_t * obj_array, const std::size_t obj_offset) override;
  int tech_start() override;
  int tech_stop() override;

  static float s0_0_callback(void);
  static float s0_1_callback(void);

private:
  APP_Frequency& m_s0_input_0;
  APP_Frequency& m_s0_input_1;
  static Tech* m_tech_ptr;
  lwm2m_power_instance_data_t m_power_data;
  lwm2m_power_instance_data_t m_power_data2;
  const uint32_t m_imp_per_kwh;
};

#endif /* TECH_H_ */