/*-----------------------------------------------------------------------------------------------*/
/*
 * Project: S0-Logger
 *
 * This Project is Licensed under the BSD-2-Clause Plus Patent License
 */
/*-----------------------------------------------------------------------------------------------*/
/* SPDX-FileCopyrightText: 2024 Albert Krenz<albert.krenz @mailbox.org> */
/* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef __WIFI_H_
#define __WIFI_H_

#include <zephyr/net/net_mgmt.h>
#include <zephyr/net/wifi_mgmt.h>

#include <vector>
#include <string>
#include <memory>
#include <util.h>

/**
 * @brief Class to simplify Wifi usage like scanning SSID's, connect/disconnect, etc.
 * 
 */
class Wifi : private Noncopyable
{
public:
    Wifi();

    /**
     * @brief Returns if Wifi Network is up and ready
     * 
     * @return true if Wifi is ready, false otherwise 
     */
    bool is_up();

    /**
     * @brief Try to connect to the specified Wifi SSID
     * 
     * @param ssid SSID name to connect to. Get available names via `Wifi::scan()`
     * @param pw Password for secured networks
     */
    void connect(const std::string_view& ssid, const std::string_view& pw = "");

    /**
     * @brief Scan for available SSID's on Wifi
     * 
     * @return std::vector<std::string> Vector of SSID names found
     */
    std::vector<std::string> scan();

    /**
     * @brief Returns if Wifi connection was successfull
     * 
     * @return true if connection was successfull. False otherwise.
     */
    bool is_connected();

    /**
     * @brief Wait for Wifi connection to succeed until timeout passes.
     * 
     * @param timeout_ms Timeout in Milliseconds
     * @return Returns true if Wifi connection was successfull, false otherwise
     */
    bool wait_for_connected(uint32_t timeout_ms=5000);

    /**
     * @brief Disable power saving mode for wifi
     * 
     */
    void disablePowerSafe(void);
    
    /**
     * @brief Enable power saving mode for wifi
     * 
     */
    void enablePowerSafe(void);

    #if defined(CONFIG_NET_DHCPV4)
    /**
     * @brief Request a new IP address via DHCP
     * 
     */
    void request_dhcp_address(void);

    /**
     * @brief Wait untill new ip address via DHCP has been received
     * 
     * @param timeout_ms Timeout in milliseconds to wait. Default = 5000ms
     * @return Returns true if an ip address has been received successfully, false otherwise.
     */
    bool wait_for_dhcp_address(uint32_t timeout_ms=5000);

    /**
     * @brief Returns if an IP address has been received via DHCP
     * 
     * @return Returns true if an ip address has been received via dhcp, false otherwise.
     */
    bool got_dhcp_address(void);
    #endif

    /**
     * @brief Get IP unicast address
     * 
     * @return std::string IP address as string of the format "192.168.0.1"
     */
    std::string get_ip(void);

    int setup_accesspoint(const std::string_view& ssid, const std::string_view& password);

private:
    struct net_mgmt_event_callback m_cb;
    struct net_mgmt_event_callback m_net_cb;
};

#endif /* __WIFI_H_ */