#! /bin/env python3
# Copyright (c) 2024 Albert Krenz <albert.krenz@mailbox.org>
# SPDX-License-Identifier: BSD-2-Clause-Patent

import json
import argparse

def extract_sources(compile_commands_path):
    with open(compile_commands_path, 'r') as f:
        compile_commands = json.load(f)

    compiled_sources = set()
    for command in compile_commands:
        compiled_sources.add(command['file'])

    for src in compiled_sources:
        print(f"{src}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Extract compiled source files from compile_commands.json.")
    parser.add_argument('compile_commands_path', type=str, help='Path to the compile_commands.json file')

    args = parser.parse_args()
    extract_sources(args.compile_commands_path)
