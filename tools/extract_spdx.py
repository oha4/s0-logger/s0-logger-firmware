#! /bin/env python3
# Copyright (c) 2024 Albert Krenz <albert.krenz@mailbox.org>
# SPDX-License-Identifier: BSD-2-Clause-Patent

import json
import argparse
import re

def extract_spdx_info(file_path, regex_list):
    result = ""
    try:
        with open(file_path, 'r') as file:
            for line in file:
                for regex in regex_list:
                    match = regex.search(line)
                    if match:
                        return match.group(1)
    except FileNotFoundError:
        return {"Error": "File not found"}
    return "No information found"

def process_file_list(file_list_path, regex_list):
    with open(file_list_path, 'r') as file_list:
        files = [line.strip() for line in file_list.readlines()]
    
    all_results = {}
    for file_path in files:
        all_results[file_path] = extract_spdx_info(file_path, regex_list)

    return all_results

def remove_end_comment(x):
    # Regex-Muster zum Entfernen von "*/" am Ende der Zeile
    remove_end_comment_regex = re.compile(r'\s*\*/$')

    # Entfernen von "*/" am Ende der Zeile, falls vorhanden
    return re.sub(remove_end_comment_regex, '', x)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Extract SPDX information from files.")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--file', type=str, help='Path to the single file to scan')
    group.add_argument('--file-list', type=str, help='Path to the file containing the list of files to scan')
    parser.add_argument('--copyright', action='store_true', help='Extract copyright information from Sourcefiles')

    args = parser.parse_args()

    # REUSE-IgnoreStart
    # Define the list of regex patterns
    license_regex = [
        re.compile(r'SPDX-License-Identifier: ([\S]+)'),
    ]

    copyright_regex = [
        re.compile(r'Copyright \(c\) (.+)', re.IGNORECASE),
        re.compile(r'Copyright © (.+)', re.IGNORECASE),
        re.compile(r'\(c\) (.+)', re.IGNORECASE),
        re.compile(r'SPDX-FileCopyrightText: ([\S\s]+)'),
    ]
    # REUSE-IgnoreEnd

    regex_list = copyright_regex if args.copyright else license_regex

    if args.file:
        # Process a single file
        result = extract_spdx_info(args.file, regex_list)
        print(f"{args.file}: {remove_end_comment(result)}")
    elif args.file_list:
        # Process a list of files
        all_results = process_file_list(args.file_list, regex_list)
        for file_path, result in all_results.items():
            print(f"File {file_path}: {remove_end_comment(result)}")
            # for key, value in results.items():
            #     print(f"  {key}: {value}")
