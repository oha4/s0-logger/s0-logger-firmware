#! /bin/env bash

# Copyright (c) 2024 Albert Krenz <albert.krenz@mailbox.org>
# SPDX-License-Identifier: BSD-2-Clause-Patent
#

JLinkExe -device NRF52840_XXAA -if SWD -speed 4000 -CommandFile $1