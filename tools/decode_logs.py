#!/bin/env python3

from pw_tokenizer import detokenize
import sys
import argparse

def extract_string(binary_string):
    length_byte = binary_string[:1]  # das erste Byte als String (Byte)
    num_elements = int.from_bytes(length_byte, 'big')  # die Länge aus dem ersten Byte extrahieren

    data = binary_string[1:1+num_elements]
    rest = binary_string[1+num_elements:]

    detok = detokenize.Detokenizer('database.csv')
    print(detok.detokenize(data))
    return rest  # der verbleibende Suffix


if __name__ == "__main__":

    # Erstellen eines Argumentparsers
    parser = argparse.ArgumentParser(description="Decode tokenized Log messages")

    parser.add_argument("-d", "--database", help="Path to a database in csv format", default='database.csv')
    parser.add_argument("binary_string", help="Der zu entokenisierende String")

    # Parse der Befehlszeile
    args = parser.parse_args()

    try:
        detok = detokenize.Detokenizer(args.database)
    except FileNotFoundError as e:
        print("Error: Database \"%s\" file not found" % (args.database))
        sys.exit(1)

    binary_str = args.binary_string
    bytes_str = bytes.fromhex(binary_str)

    tmp = bytes_str
    while True:
        tmp = extract_string(tmp)
        if not tmp:  # wenn kein Rest mehr zurückgeliefert wird
            break