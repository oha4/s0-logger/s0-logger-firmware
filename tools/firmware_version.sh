#! /bin/bash

# Copyright (c) 2024 Albert Krenz <albert.krenz@mailbox.org>
# SPDX-License-Identifier: BSD-2-Clause-Patent
#

version=$(git describe --tags)

# Die einzelnen Teile der Version extrahieren
if [[ $version == *"-"* ]]; then
    build=$(echo $version | cut -d- -f2)
else
    build=""
fi

major=$(echo $version | cut -d. -f1 | sed 's/v//')  # Entfernt das "v" am Anfang
minor=$(echo $version | cut -d. -f2)
patch=$(echo $version | cut -d. -f3 | cut -d- -f1)
build_hash=$(git rev-parse --short HEAD)

if [ -z $build ]; then
echo "$major.$minor.$patch"
else
echo "$major.$minor.$patch+$build"
fi
